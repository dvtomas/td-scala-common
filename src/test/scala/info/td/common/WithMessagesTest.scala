package info.td.common

import java.util.Date

import info.td.common.ui.UIMessage
import info.td.testhelpers.TdTestSuite
import utest._

object WithMessagesTest extends TdTestSuite {
  private[this] def msg(s: String) = new UIMessage(s, UIMessage.Info, new Date(0))

  val tests: Tests = Tests {
    test("map") {
      assertEquals(
        WithMessages(1, msg("Foo")) map (_ + 1),
        WithMessages(2, msg("Foo"))
      )
    }

    test("flatMap") {
      assertEquals(
        WithMessages(1, msg("Foo")) flatMap (x => WithMessages(x + 1, msg("Bar"))),
        WithMessages(2, msg("Foo") :: msg("Bar") :: Nil)
      )
    }

    test("mapOption") {
      assertEquals(
        WithMessages(Some(1), msg("Foo")) mapOption ((_: Int) + 1),
        WithMessages(Some(2), msg("Foo"))
      )
    }

    test("flatMapOption") {
      assertEquals(
        WithMessages(Some(1), msg("Foo")) flatMapOption ((x: Int) => WithMessages(Some(x + 1), msg("Bar"))),
        WithMessages(Some(2), List(msg("Foo"), msg("Bar")))
      )
    }

    test("flatMapSeq") {
      assertEquals(
        WithMessages(List(1, 2), msg("Foo")) flatMapSeq ((x: Int) => WithMessages(List(x + "a", x + "b"), msg(s"Bar $x"))),
        WithMessages(List("1a", "1b", "2a", "2b"), msg("Foo") :: msg("Bar 1") :: msg("Bar 2") :: Nil))
    }

    test("flatMapSeq with Streams") {
      assertEquals(
        WithMessages(LazyList(1, 2), msg("Foo")) flatMapSeq ((x: Int) => WithMessages(LazyList(x + "a", x + "b"), msg(s"Bar $x"))),
        WithMessages(List("1a", "1b", "2a", "2b"), msg("Foo") :: msg("Bar 1") :: msg("Bar 2") :: Nil))
    }

    test("withAnotherMessage") {
      assertEquals(
        WithMessages(42, msg("Foo")) withAnotherMessage msg("Bar"),
        WithMessages(42, msg("Foo") :: msg("Bar") :: Nil)
      )
    }

    test("withClearedMessages") {
      assertEquals(
        WithMessages(42, msg("Foo")).withClearedMessages,
        WithMessages(42, Nil)
      )
    }

    test("traverse") {
      val result: WithMessages[Seq[String]] =
        WithMessages.traverse(List((0, "a"), (1, "b"), (2, "c")))(x =>
          WithMessages(x._2, (1 to x._1).toList.map(i => msg(s"${x._2}${i}")))
        )

      assertEquals(result, WithMessages(Seq("a", "b", "c"), List(msg("b1"), msg("c1"), msg("c2"))))
    }

    test("traverse with Stream") {
      val result: WithMessages[Seq[String]] =
        WithMessages.traverse(LazyList((0, "a"), (1, "b"), (2, "c")))(x =>
          WithMessages(x._2, (1 to x._1).toList.map(i => msg(s"${x._2}${i}")))
        )

      assertEquals(result, WithMessages(Seq("a", "b", "c"), List(msg("b1"), msg("c1"), msg("c2"))))
    }
  }
}
