package info.td.common.string

import info.td.testhelpers.TdTestSuite
import utest._

object StringHelpersTest extends TdTestSuite {
  val tests: Tests = Tests {
    test("etcTo") {
      assertEquals(StringHelpers.etcTo("1", 10), "1")
      assertEquals(StringHelpers.etcTo("1", 0), "1")
      assertEquals(StringHelpers.etcTo("123", 3), "123")
      assertEquals(StringHelpers.etcTo("123", 1), "123")
      assertEquals(StringHelpers.etcTo("1234", 3), "1234")
      assertEquals(StringHelpers.etcTo("123456", 4), "1...")
      assertEquals(StringHelpers.etcTo("123456", 5), "12...")
      assertEquals(StringHelpers.etcTo("123456", 6), "123456")
    }
  }
}
