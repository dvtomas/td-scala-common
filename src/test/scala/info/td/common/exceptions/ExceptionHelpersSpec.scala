package info.td.common.exceptions

import info.td.common.exceptions.ExceptionHelpers._
import org.specs2._
import org.specs2.specification.core.SpecStructure

class ExceptionHelpersSpec extends Specification {
  val inner = new RuntimeException("inner")
  val middle = new RuntimeException("middle", inner)
  val outer = new RuntimeException("outer", middle)

  def is: SpecStructure = "ExceptionHelpers should" ^
    "exceptionList unfolds the exceptions chain, outer(middle(inner)) => (outer, middle, inner)" ! {
      outer.causalList must beEqualTo(outer :: middle :: inner :: Nil)
    }
}