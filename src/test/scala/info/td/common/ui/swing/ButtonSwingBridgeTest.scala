package info.td.common.ui.swing

import utest._

object ButtonSwingBridgeTest extends utest.TestSuite {
  val tests: Tests = Tests {
    test("test") {
      val b = new ButtonSwingBridge("Foo")
      assert(b.component.getActionListeners.isEmpty)
      b.component.doClick()

      var counter1 = 0
      var counter2 = 0

      val s1 = b.clicks.subscribe(_ => counter1 += 1)
      assert(counter1 == 0, counter2 == 0)

      b.component.doClick()
      assert(counter1 == 1, counter2 == 0)

      val s2 = b.clicks.subscribe(_ => counter2 += 1)
      assert(counter1 == 1, counter2 == 0)

      b.component.doClick()
      assert(counter1 == 2, counter2 == 1)

      s1.unsubscribe()
      b.component.doClick()
      assert(counter1 == 2, counter2 == 2)

      s2.unsubscribe()
      b.component.doClick()
      assert(counter1 == 2, counter2 == 2)

      val s3 = b.clicks.subscribe(_ => counter1 += 1)
      val s4 = b.clicks.subscribe(_ => counter2 += 1)
      b.component.doClick()
      assert(counter1 == 3, counter2 == 3)

      s3.unsubscribe()
      s4.unsubscribe()
      b.component.doClick()
      assert(counter1 == 3, counter2 == 3)

      assert(b.component.getActionListeners.isEmpty) // when no one is subscribed to the clicks, there should be no ActionListeners registered.
    }
  }
}