package info.td.common.rx

import info.td.common.rx.RxHelpers._
import rx.lang.scala._
import utest._

import scala.concurrent.duration._

object ObserveLatestOnTest extends utest.TestSuite {
  private[this] def observeLatestOn[T](o: Observable[T], scheduler: Scheduler): Observable[T] = {
    o.observeLatestOn(scheduler)
  }

  val tests: Tests = Tests {
    implicit val schedulerProvider: SchedulerProvider = new TestingSchedulerProvider
    //    implicit val schedulerProvider = productionSchedulerProvider
    val timeSlice = 100.milli


    test("ObserveLatestOn") {
      val scheduler = schedulerProvider.computation

      test("zx swing test") {
        val xs = collection.mutable.ArrayBuffer[Long]()
        var completed = false

        val o = Observable
          .interval(timeSlice, scheduler)
          .take(10)

        observeLatestOn(o, scheduler)
          .subscribe(v => {
            schedulerProvider.advanceTimeBy(timeSlice * 2)
            xs += v
          },
            e => e.printStackTrace(),
            () => completed = true
          )
        schedulerProvider.advanceTimeBy(timeSlice * 11)
        assert(completed)
        val xsList = xs.toList
        assert(xsList == List(0, 2, 4, 6, 8))
      }
    }
  }
}

object BufferIntrospectiveTest extends utest.TestSuite {
  private[this] def bufferIntrospective[T](o: Observable[T], scheduler: Scheduler): Observable[Seq[T]] = {
    o.bufferIntrospective(sendFinalBufferedItemsBeforeCompletion = true, scheduler)
  }

  val tests: Tests = Tests {
    implicit val schedulerProvider: SchedulerProvider = new TestingSchedulerProvider
    //        implicit val schedulerProvider = productionSchedulerProvider
    val timeSlice = 100.milli


    test("BufferIntrospective") {
      val scheduler = schedulerProvider.computation

      test("*") {
        val xs = collection.mutable.ArrayBuffer[Seq[Long]]()
        var completed = false

        val o = Observable
          .interval(timeSlice, scheduler)
          .take(10)

        bufferIntrospective(o, scheduler)
          .subscribe(v => {
            schedulerProvider.advanceTimeBy(timeSlice * 3.1)
            xs += v
          },
            e => e.printStackTrace(),
            () => completed = true
          )
        schedulerProvider.advanceTimeBy(timeSlice * 15)
        assert(completed)
        val xsList = xs.map(_.toList)
        assert(xsList.size == 4)
        assert(xsList.head == List(0))
        assert(xsList(1) == List(1, 2, 3))
        assert(xsList(2) == List(4, 5, 6))
        assert(xsList(3) == List(7, 8, 9))
      }
    }
  }
}



