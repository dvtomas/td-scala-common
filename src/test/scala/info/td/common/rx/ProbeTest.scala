package info.td.common.rx

import info.td.common.rx.RxHelpers._
import rx.lang.scala._
import utest._

import java.io.{PrintWriter, StringWriter}
import scala.concurrent.duration._
import scala.util.Try

object ProbeTest extends utest.TestSuite {
  implicit val schedulerProvider: SchedulerProvider = productionSchedulerProvider

  val tests: Tests = Tests {
    // This test is failing since the scala-2.13 tag. I don't know how to fix it again.
    // Even before, the probe on the stack trace itself wasn't that useful. The stack frame line looked e.g. like this:
    // at info.td.common.rx.ProbeTest$$anonfun$11$$anonfun$apply$19$$anonfun$apply$20$$anonfun$13.apply(ObserveLatestOnTest.scala:105) [test-classes/:na]
    // 	at info.td.common.rx.RxObservableExtensions$ObservableWrapper$$anonfun$probe$1$$anon$2.onNext(RxObservableExtensions.scala:290) [classes/:na]
    // You see that it does mention the $probe$ method being called (which it does NOT do after the update),
    // but the name of the probe is missing from the stack


    // I don't really remember how was it supposed to work, and don't have time to investigate now.
    test("*:") {
      val o1 =
        Observable
          .interval(1.milli, schedulerProvider.newThread)
          .probe("Fast interval", _ (_))
          .map(_ + 1)
          .probe("o1", _ (_))

      val o2 =
        Observable
          .interval(1000.milli, schedulerProvider.newThread)
          .probe("o2", _ (_))

      val resultingException = Try {
        o1.zip(o2)
          .probe("zipped", _ (_))
          .take(100)
          .map(_ => schedulerProvider.advanceTimeBy(500.milli))
          .probe("final", _ (_))
          .toBlocking.toList
      }.failed.get

      val stackTrace = {
        val sw = new StringWriter()
        resultingException.printStackTrace(new PrintWriter(sw))
        sw.toString
      }

      // ???
      //      assert(stackTrace contains "o1")
      //      assert(stackTrace contains "o2")
      //      assert(stackTrace contains "Fast interval")
      //      assert(stackTrace contains "zipped")
      //      assert(stackTrace contains "final")
      assert(stackTrace contains "ProbeTest$")
    }
  }
}
