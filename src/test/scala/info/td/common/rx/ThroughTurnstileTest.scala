package info.td.common.rx

import info.td.common.rx.RxHelpers._
import rx.lang.scala.Subject
import utest._

object ThroughTurnstileTest extends TestSuite {
  val tests: Tests = Tests {
    val queue = Subject[String]()
    val turnstile = Subject[Boolean]()
    var lastPassed: (Int, String) = (0, "")


    // Alternative implementation from http://stackoverflow.com/questions/27797609/implementing-a-turnstile-like-operator-with-rxjava
    /*
        def throughTurnstile(queue: Observable[String], tokens: Observable[Boolean]) =
            queue.publish(sharedQueue => tokens.switchMap(token => if (token) sharedQueue.take(1) else Observable.empty))

        throughTurnstile(queue, turnstile).subscribe(next => lastPassed = (lastPassed._1 + 1, next))
    */

    queue.throughTurnstile(turnstile).subscribe(next => lastPassed = (lastPassed._1 + 1, next))

    test("Don't let anyone until turnstile becomes true") {
      turnstile onNext false
      queue onNext "Foo"
      turnstile onNext true
      assert(lastPassed == (1, "Foo"))
    }

    test("Don't let anyone until turnstile becomes true") {
      turnstile onNext false
      assert(lastPassed == (0, ""))
      queue onNext "Foo"
      assert(lastPassed == (0, ""))
      turnstile onNext true
      assert(lastPassed == (1, "Foo"))
    }

    test("Only let one at a time") {
      turnstile onNext false
      assert(lastPassed == (0, ""))
      queue onNext "Foo"
      queue onNext "Bar"
      queue onNext "Baz"
      assert(lastPassed == (0, ""))
      turnstile onNext true
      assert(lastPassed == (1, "Foo"))
      turnstile onNext true
      assert(lastPassed == (2, "Bar"))
      turnstile onNext false
      assert(lastPassed == (2, "Bar"))
      turnstile onNext true
      assert(lastPassed == (3, "Baz"))
    }

    test("Let the element pass immediately if the turnstile is open") {
      turnstile onNext true
      assert(lastPassed == (0, ""))
      queue onNext "Foo"
      assert(lastPassed == (1, "Foo"))
      queue onNext "Bar"
      assert(lastPassed == (1, "Foo"))
    }

    test("Decline elements when the turnstile is closed") {
      queue onNext "Foo"
      turnstile onNext true
      assert(lastPassed == (1, "Foo"))
      turnstile onNext true
      turnstile onNext false
      queue onNext "Bar"
      assert(lastPassed == (1, "Foo"))
      turnstile onNext true
      assert(lastPassed == (2, "Bar"))
    }

    test("Turnstile should not save slots for future. If it is open and no one is waiting in the queue, the slot is disregarded ") {
      turnstile onNext true
      assert(lastPassed == (0, ""))
      queue onNext "Foo"
      assert(lastPassed == (1, "Foo"))
      turnstile onNext false
      turnstile onNext true
      turnstile onNext true
      turnstile onNext false
      queue onNext "Bar"
      queue onNext "Baz"
      turnstile onNext true
      assert(lastPassed == (2, "Bar"))
    }
  }
}