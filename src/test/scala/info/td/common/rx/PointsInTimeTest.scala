package info.td.common.rx

import info.td.common.rx.RxHelpers._
import info.td.testhelpers.TdTestSuite
import rx.lang.scala.subjects.ReplaySubject
import utest._

import scala.concurrent.duration._

object PointsInTimeTest extends TdTestSuite {
  val tests: Tests = Tests {
    test("test") {
      implicit val schedulerProvider: SchedulerProvider = new TestingSchedulerProvider

      val resultSubject = ReplaySubject[(Long, Long)]()

      pointsInTimeMilliSeconds(Seq(100, 300, 300, 500), schedulerProvider.trampoline)
        .timestampedFromNow
        .subscribeToSubject(resultSubject)

      schedulerProvider.advanceTimeBy(500.milli)

      assertEquals(resultSubject.toBlocking.toList, Seq((100, 100), (300, 300), (300, 300), (500, 500)))
    }
  }
}