package info.td.testhelpers

import utest._

import scala.reflect.ClassTag

abstract class TdTestSuite extends TestSuite {
  protected[this] def assertEquals[T : ClassTag](a: T, b: T): Unit = {
    assert(a == b)
  }
}