package info.td.virtualkeyboard

import java.awt.event._
import java.awt.{List => _, _}
import javax.swing._
import javax.swing.text.JTextComponent

import com.typesafe.scalalogging.LazyLogging
import info.td.common._
import info.td.common.ui.UIMessage
import info.td.common.ui.swing.{JFrameHelpers, SwingHelpers}
import net.miginfocom.swing.MigLayout

case class KeyboardDefinition(isNumeric: Boolean, charRows: Seq[Seq[(Char, Char)]])

case class KeyboardAppearance(keyFontSize: Int = 20, textFontSize: Int = 16) {
  val keyFont = new Font("Monospaced", Font.BOLD, keyFontSize)
  val textFont = new Font("Monospaced", Font.BOLD, textFontSize)
}

object KeyboardDefinition {
  def parseFromString(isNumeric: Boolean, definitionString: String): WithMessages[KeyboardDefinition] = {

    WithMessages.traverse(definitionString.lines.filter(_.nonEmpty).toVector) { line ⇒
      val charPairsWithMessages: WithMessages[Seq[(Char, Char)]] =
        WithMessages.traverse(line.split(' ')) { charPair ⇒
          if (charPair.length == 2)
            WithMessages(Seq((charPair(0), charPair(1))))
          else
            WithMessages(Seq.empty, UIMessage.error(s"Expected a pair of characters, line $line, characters $charPair"))
        }.map(_.flatten)
      charPairsWithMessages
    }
      .map(KeyboardDefinition(isNumeric, _))
  }

  lazy val usKeyboard: KeyboardDefinition = parseFromString(isNumeric = false,
    """
`~ 1! 2@ 3# 4$ 5% 6^ 7& 8* 9( 0) -_ =+
qQ wW eE rR tT yY uU iI oO pP [{ ]}
aA sS dD fF gG hH jJ kK lL ;: '" \|
zZ xX cC vV bB nN mM ,< .> /?"""
  ).value

  lazy val numericKeyboard: KeyboardDefinition = parseFromString(isNumeric = true,
    """
77 88 99
44 55 66
11 22 33
00 .. --"""
  ).value
}

private object KeyboardState {

  abstract sealed class ShiftState

  case object Regular extends ShiftState

  case object Shift extends ShiftState

  case object CapsLock extends ShiftState

}

private class KeyboardState(val previewComponent: JTextComponent, val isTextArea: Boolean) {

  import info.td.virtualkeyboard.KeyboardState._

  private var shiftState: ShiftState = Regular

  def isShift: Boolean = shiftState != Regular

  def getShiftState: ShiftState = shiftState

  def setShiftState(newState: ShiftState): Unit = {
    shiftState = newState
  }

  def goToRegularIfShift(): Unit = {
    if (shiftState == Shift)
      shiftState = Regular
  }
}

private class SwingVirtualKeyboard(
  textComponent: JTextComponent,
  description: String,
  keyboardDefinition: KeyboardDefinition,
  appearance: KeyboardAppearance,
  selection: (Int, Int)) extends LazyLogging {
  private var allKeys: List[Key] = Nil

  abstract class Key {
    def clickAction()

    protected def autoRepeat() {
      jButton.addMouseListener(new MouseAdapter() {

        var repeatTimer = new Timer(200, new ActionListener() {
          def actionPerformed(actionEvent: ActionEvent): Unit = {
            clickAction()
          }
        })
        repeatTimer.setInitialDelay(500)

        override def mousePressed(event: MouseEvent) {
          repeatTimer.start()
        }

        override def mouseReleased(event: MouseEvent) {
          repeatTimer.stop()
        }
      })
    }

    val jButton: AbstractButton = {
      val button: AbstractButton = createButton
      button.addActionListener(new ActionListener {
        def actionPerformed(actionEvent: ActionEvent): Unit = {
          keyboardState.previewComponent.grabFocus()
          clickAction()
          keyboardState.previewComponent.grabFocus()
        }
      })
      button
    }

    protected def createButton: AbstractButton = {
      val button = new JButton("")
      button.setFont(appearance.keyFont)
      button.setMargin(new Insets(4, 5, 4, 5))
      button
    }

    def updateAppearance(): Unit
  }

  private class OrdinaryKey(regularLabel: String, regularChar: Char, withShiftLabel: String, withShiftChar: Char) extends Key {
    def clickAction() {
      keyboardState.previewComponent.dispatchEvent(
        new KeyEvent(keyboardState.previewComponent, KeyEvent.KEY_TYPED, 0, 0, KeyEvent.VK_UNDEFINED, if (keyboardState.isShift) withShiftChar else regularChar)
      )
      keyboardState.goToRegularIfShift()
      allKeys foreach (_.updateAppearance())
    }

    def updateAppearance(): Unit = {
      jButton.setText(if (keyboardState.isShift) withShiftLabel else regularLabel)
    }

    autoRepeat()
  }

  private class SpecialKey(label: String, vkCode: Int, shouldAutoRepeat: Boolean) extends Key {
    def clickAction(): Unit = keyboardState.previewComponent.dispatchEvent(
      new KeyEvent(keyboardState.previewComponent, KeyEvent.KEY_PRESSED, 0, 0, vkCode, 0)
    )

    def updateAppearance() {
      jButton.setText(label)
    }

    if (shouldAutoRepeat)
      autoRepeat()
  }

  private class SpecialKeyWithImage(image: ImageIcon, vkCode: Int, shouldAutoRepeat: Boolean) extends Key {
    def updateAppearance(): Unit = {
      jButton.setIcon(image)
    }

    def clickAction(): Unit = keyboardState.previewComponent.dispatchEvent(
      new KeyEvent(keyboardState.previewComponent, KeyEvent.KEY_PRESSED, 0, 0, vkCode, 0)
    )

    if (shouldAutoRepeat)
      autoRepeat()
  }

  private case class OrdinaryKeysRow(keysDefinitions: Seq[(Char, Char)]) {
    def toJPanel: JPanel = {
      val panel = new JPanel(new MigLayout("fill,gap 0,insets 0"))
      for (keyDefinition <- keysDefinitions) {
        val key = new OrdinaryKey(keyDefinition._1.toString, keyDefinition._1, keyDefinition._2.toString, keyDefinition._2)
        allKeys ::= key
        panel.add(key.jButton, "grow")
        if (keyboardDefinition.isNumeric) {
          key.jButton setMargin new Insets(8, 20, 8, 20)
        }
      }
      panel
    }
  }

  private def ordinaryKeysPanel: JPanel = {
    val panel = new JPanel(new MigLayout("fill,gap 0,insets 0", "[fill]"))
    keyboardDefinition.charRows.zipWithIndex foreach { case (row, index) =>
      panel.add(OrdinaryKeysRow(row).toJPanel, s"growY${if (index == 0) "" else ",newline"}")
    }
    if (!keyboardDefinition.isNumeric) {
      val spaceKey = new OrdinaryKey(" ", ' ', " ", ' ')
      allKeys ::= spaceKey
      panel.add(spaceKey.jButton, "newline, growY")
    }
    panel
  }

  private[this] def applyKey(icon: ImageIcon) = new Key {
    def clickAction(): Unit = {
      performEnter()
    }

    def updateAppearance(): Unit = {
      jButton.setIcon(icon)
    }
  }


  private def leftKeysPanel: JPanel = {
    object escapeKey extends Key {
      def clickAction(): Unit = close()

      def updateAppearance() {
        jButton.setFont(appearance.textFont)
        jButton.setText(" Esc ")
      }
    }

    object shiftKey extends Key {
      override protected def createButton = new JToggleButton(SwingHelpers.imageIcon("/images/shift-up.png"))

      override def updateAppearance(): Unit = {
        if (keyboardState.getShiftState == KeyboardState.Shift)
          jButton.setSelected(true)
        else
          jButton.setSelected(false)
      }

      def clickAction() {
        keyboardState.setShiftState(if (jButton.isSelected) KeyboardState.Shift else KeyboardState.Regular)
        allKeys foreach (_.updateAppearance())
      }
    }

    object capsLockKey extends Key {
      override protected def createButton = new JToggleButton(SwingHelpers.imageIcon("/images/capsLock.png"))

      override def updateAppearance(): Unit = {
        if (keyboardState.getShiftState == KeyboardState.CapsLock)
          jButton.setSelected(true)
        else
          jButton.setSelected(false)
      }

      def clickAction() {
        keyboardState.setShiftState(if (jButton.isSelected) KeyboardState.CapsLock else KeyboardState.Regular)
        allKeys foreach (_.updateAppearance())
      }
    }

    if (keyboardDefinition.isNumeric) {
      val panel = new JPanel(new MigLayout("fill,gap 0,insets 0,wrap", "[fill]", "[fill]"))
      panel.add(escapeKey.jButton)
      allKeys ::= escapeKey
      panel
    } else {
      val panel = new JPanel(new MigLayout("fill,gap 0,insets 0,wrap", "[fill]", "[fill][fill][fill]"))
      panel.add(escapeKey.jButton)
      allKeys ::= escapeKey
      panel.add(capsLockKey.jButton)
      allKeys ::= capsLockKey
      panel.add(shiftKey.jButton)
      allKeys ::= shiftKey
      panel
    }
  }

  private def updateComponent(source: JTextComponent, target: JTextComponent): Unit = {
    target.setText(source.getText)
    target.setCaretPosition(source.getCaretPosition)
  }

  private def rightKeysPanel: JPanel = {
    val panel = new JPanel(new MigLayout(
      "fill,gap 0,insets 0,wrap",
      "[fill,sizeGroup]0[fill,sizeGroup]",
      s"[fill][fill][fill]"))

    val deleteKey = new SpecialKey(" Del ", KeyEvent.VK_DELETE, true)
    deleteKey.jButton.setFont(appearance.textFont)
    panel.add(deleteKey.jButton)
    allKeys ::= deleteKey

    val backspaceKey = new SpecialKeyWithImage(SwingHelpers.imageIcon("/images/backspace.png"), KeyEvent.VK_BACK_SPACE, true)
    panel.add(backspaceKey.jButton)
    allKeys ::= backspaceKey

    val enterKey = if (keyboardState.isTextArea) {
      new SpecialKeyWithImage(SwingHelpers.imageIcon("/images/newline.png"), KeyEvent.VK_ENTER, true)
    } else {
      applyKey(SwingHelpers.imageIcon("/images/newline.png"))
    }

    panel.add(enterKey.jButton, "span")
    allKeys ::= enterKey

    val leftKey = new SpecialKeyWithImage(SwingHelpers.imageIcon("/images/left.png"), KeyEvent.VK_LEFT, true)
    panel.add(leftKey.jButton)
    allKeys ::= leftKey
    val rightKey = new SpecialKeyWithImage(SwingHelpers.imageIcon("/images/right.png"), KeyEvent.VK_RIGHT, true)
    panel.add(rightKey.jButton)
    allKeys ::= rightKey

    panel
  }

  private def keyboardPanel: JPanel = {
    val panel = new JPanel(new MigLayout("fill,gap 0, insets 0", "[][grow 60,fill][]", "[grow,fill]"))
    panel.add(leftKeysPanel)
    panel.add(ordinaryKeysPanel)
    panel.add(rightKeysPanel)

    panel
  }

  def mainPanel: JPanel = {
    val panel = new JPanel()

    val enterKey = applyKey(SwingHelpers.imageIcon("/images/apply.png"))

    panel.setLayout(new MigLayout(s"insets 1, wrap", "[fill,grow]", s"[]${
      if (isTextArea) "[grow, fill]" else "[]"
    }[grow,fill]"))
    panel.add(descriptionLabel)
    panel.add(editBoxContainer, "split, grow")
    editBox.addKeyListener(new KeyListener {
      def keyTyped(keyEvent: KeyEvent) {
      }

      def keyPressed(keyEvent: KeyEvent): Unit = {
        if (keyEvent.getKeyChar == '\n' && (!keyboardState.isTextArea || (keyEvent.getModifiers & ActionEvent.CTRL_MASK) != 0)) {
          performEnter()
        }
      }

      def keyReleased(keyEvent: KeyEvent): Unit = {
      }
    }

    )
    panel.add(enterKey.jButton, "wrap")
    allKeys ::= enterKey


    updateComponent(textComponent, editBox)
    editBox.setSelectionStart(selection._1)
    editBox.setSelectionEnd(selection._2)

    panel.add(keyboardPanel)
    panel
  }

  private def performEnter() {
    updateComponent(keyboardState.previewComponent, textComponent)
    textComponent.setSelectionStart(editBox.getSelectionStart)
    textComponent.setSelectionEnd(editBox.getSelectionEnd)
    close()
  }

  private def isTextArea = textComponent match {
    case _: JTextField => false
    case _ => true
  }

  private val descriptionLabel: JLabel = {
    val label = new JLabel(description)
    val oldFont = label.getFont
    val descriptionFont = oldFont.deriveFont(appearance.textFontSize.toFloat)
    label.setFont(descriptionFont)
    label
  }

  private val (editBox: JTextComponent, editBoxContainer: JComponent) = {
    if (isTextArea) {
      val component = new JTextArea()
      component.setLineWrap(true)
      //      component.setFont(descriptionFont)
      val scrollPane = new JScrollPane(component)
      (component, scrollPane)
    } else {
      val component = new JTextField()
      //      component.setFont(descriptionFont)
      (component, component)
    }
  }

  private val keyboardState = new KeyboardState(editBox, isTextArea)

  private[this] def owner = SwingHelpers.findWindowOf(textComponent).getOrElse {
    logger.warn("Can't find owner of the text component")
    null
  }

  object dialog extends JDialog(owner, i18n.t("Virtual keyboard"), java.awt.Dialog.ModalityType.APPLICATION_MODAL) with JFrameHelpers {
    val panel: Container = getContentPane
    panel.setLayout(new MigLayout("fill,gap 0,insets 0", "[fill,grow]", "[fill,grow]"))
    panel.add(mainPanel)

    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)

    closeOnLostFocus()
    closeOnEscape()
  }

  def openKeyboardFrame(maybeMousePosition: Option[Point]): Unit = {
    SwingHelpers.invokeOnEDT {
      dialog.revalidate()
      allKeys.foreach(_.updateAppearance())
      dialog.pack()
      dialog.setMinimumSize(dialog.getSize)
      val maximumBounds = GraphicsEnvironment.getLocalGraphicsEnvironment.getMaximumWindowBounds
      val height = dialog.getSize.height min maximumBounds.height // This is especially important when the multi-line text has many lines.
      if (keyboardDefinition.isNumeric) {
        dialog.setSize(new Dimension(dialog.getSize.getWidth.toInt, height))
        maybeMousePosition foreach { mousePosition =>
          SwingHelpers.setLocationTo(dialog, mousePosition)
        }
      } else {

        dialog.setSize(new Dimension(maximumBounds.width, height))
        maybeMousePosition foreach { mousePosition =>
          SwingHelpers.setLocationTo(dialog, new Point(0, mousePosition.y))
        }
      }
      dialog.setVisible(true)
    }
  }

  def close(): Unit = {
    dialog.dispatchEvent(new WindowEvent(dialog, WindowEvent.WINDOW_CLOSING))
  }
}

object VirtualKeyboard {
  def popupUSWhenClicked(
    component: JTextComponent,
    description: String,
    appearance: KeyboardAppearance = KeyboardAppearance(24, 18)
  ): Unit = {
    popupWhenClicked(component, description, KeyboardDefinition.usKeyboard, appearance)
  }

  def popupNumericWhenClicked(
    component: JTextComponent,
    description: String,
    appearance: KeyboardAppearance = KeyboardAppearance(48, 18)
  ): Unit = {
    popupWhenClicked(component, description, KeyboardDefinition.numericKeyboard, appearance: KeyboardAppearance)
  }

  def popupWhenClicked(
    component: JTextComponent,
    description: String, keyboardDefinition: KeyboardDefinition,
    appearance: KeyboardAppearance = KeyboardAppearance(24, 18)
  ): Unit = {
    component.addMouseListener(new MouseListener {
      def mouseExited(mouseEvent: MouseEvent) {}

      def mouseClicked(mouseEvent: MouseEvent): Unit = {}

      def mouseEntered(mouseEvent: MouseEvent) {}

      def mousePressed(mouseEvent: MouseEvent): Unit = {}

      def mouseReleased(mouseEvent: MouseEvent): Unit = {
        new SwingVirtualKeyboard(
          component, description, keyboardDefinition, appearance,
          (component.getSelectionStart, component.getSelectionEnd)
        ).openKeyboardFrame(Some(mouseEvent.getLocationOnScreen))
      }
    })
  }
}

object SwingVirtualKeyboardTest {
  def main(args: Array[String]): Unit = {
    val frame = new JFrame()
    val panel = frame.getContentPane
    panel.setLayout(new MigLayout("wrap", "[][fill,grow]", "[][][fill,grow]"))

    panel.add(new JLabel("Name"))
    val nameTextField = new JTextField("")
    VirtualKeyboard.popupUSWhenClicked(nameTextField, "Name")
    panel.add(nameTextField)

    panel.add(new JLabel("Size"))
    val sizeTextField = new JTextField("")
    VirtualKeyboard.popupNumericWhenClicked(sizeTextField, "Size")
    panel.add(sizeTextField)

    panel.add(new JLabel("Info"))
    val infoTextArea = new JTextArea("Blah blah")
    infoTextArea.setLineWrap(true)
    val scrollPane = new JScrollPane(infoTextArea)

    VirtualKeyboard.popupUSWhenClicked(infoTextArea, "Fill in the info")
    panel.add(scrollPane)

    frame.setMinimumSize(new Dimension(636, 450))
    frame.setTitle("Virtual keyboard demo")
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
    frame.setState(Frame.NORMAL)
    frame.setVisible(true)
  }
}
