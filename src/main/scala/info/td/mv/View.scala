package info.td.mv

import info.td.common.rx.RxHelpers._

abstract class Model

abstract class ViewSources

class NullViewSources extends ViewSources

class NullModel extends Model

trait HasViewSources[VS <: ViewSources] {
  def viewSources: VS
}

trait HasSubscribeToModel[M <: Model] {
  def subscribeToModel(model: M)(implicit schedulerProvider: SchedulerProvider)
}

/** The instantiation and subscription are separated for basically these reasons:
  *
  * - It mimics the Rx approach, where we create an observer, and subscribe to it, separately
  * - The view/model works as a cycle. The view provides view sources (e.g. a button produces clicks, input lines produce texts entered by the user. These are then transformed into a model, that the view subscribes to and reflects it's state accordingly. In order to break the loop between the view sources that are transformed into the model that the view subscribes to, we need to split it into a two phase process. Thus, the first phase occurs in the initialization, the view is prepared and the view sources are generated. These can be used to generate the model, and finally, phase two, the view subscribes to the model.
  *
  * TODO subscribeToModel should return a subscription that could be disposed when the view is no longer needed.
  *
  * @tparam M Model of the view
  * @tparam VS View sources
  */
trait View[M <: Model, VS <: ViewSources] extends HasViewSources[VS] with HasSubscribeToModel[M]
