package info.td.common.exceptions

import com.typesafe.scalalogging.Logger

import scala.util.{Success, Failure, Try}
import scala.reflect.ClassTag
import java.io.{StringWriter, PrintWriter}

object ExceptionHelpers {

  implicit class TryWrapper[T](val trial: Try[T]) extends AnyVal {
    def wrapFailure(message: => String): Try[T] = trial match {
      case success@Success(_) => success
      case Failure(cause) => Failure(UserError(message, cause))
    }

    def mapFailure(mapFunc: Throwable => Throwable): Try[T] = trial match {
      case success@Success(_) => success
      case Failure(cause) => Failure(mapFunc(cause))
    }

    def doWithFailure(doFunc: Throwable => Any): Try[T] = trial match {
      case success@Success(_) => success
      case failure @ Failure(cause) => {
        doFunc(cause)
        failure
      }
    }

    def logFailure(logger: Logger, message: String): Try[T] = {
      trial match {
        case Success(_) =>
        case Failure(error) => logger.error(message, error)
      }
      trial
    }
  }

  implicit class ExceptionWrapper(val e: Throwable) extends AnyVal {
    private def causalListRec(recursiveError: Throwable): List[Throwable] = {
      recursiveError :: (recursiveError.getCause match {
        case null => Nil
        case cause => causalListRec(cause)
      })
    }

    def causalList: List[Throwable] = causalListRec(e)

    def doesChainContain(string: String): Boolean = {
      causalList.exists(_.getMessage == string)
    }

    def doesChainContainSubstring(string: String): Boolean = {
      causalList.exists(throwable => Option(throwable.getMessage).exists(_.contains(string)))
    }

    def doesChainContainType[ExceptionType](implicit tag: ClassTag[ExceptionType]): Boolean = {
      causalList.exists(error => tag.runtimeClass.isInstance(error))
    }

    def isUserError: Boolean = e.isInstanceOf[UserError]

    def stackTraceString: String = {
      val sw = new StringWriter()
      e.printStackTrace(new PrintWriter(sw))
      sw.toString
    }
  }

}

case class UserError(message: String, cause: Throwable = null) extends Exception(message, cause) {

  import info.td.common.exceptions.ExceptionHelpers._

  override def toString: String = if (getCause == null)
    super.toString
  else
    s"${super.toString}(${getCause.causalList})"
}

