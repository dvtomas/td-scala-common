package info.td.common.collection.mutable

import scala.reflect.ClassTag

class RingBuffer[A: ClassTag](val maxSize: Int) {
  private[this] val array = new Array[A](maxSize)
  private[this] var first_ = 0
  private[this] var last_ = 0

  /** First (inclusive) */
  final def first: Int = first_

  /** Index of last element (non-inclusive) */
  final def last: Int = last_

  final def apply(i: Int): A = {
    if (i < first_) {
      throw new IndexOutOfBoundsException(s"RingBuffer: Index too small($i/$first_)")
    }
    if (i >= last_) {
      throw new IndexOutOfBoundsException(s"RingBuffer: Index too big($i/$last_)")
    }
    array(i % maxSize)
  }

  final def clear(): Unit = {
    first_ = 0
    last_ = 0
  }

  final def trimEnd(newEnd: Int): Unit = {
    if (newEnd >= last_)
      throw new IndexOutOfBoundsException(s"RingBuffer: Index too big($newEnd/$last_)")

    last_ = newEnd
    if (first_ >= last_)
      first_ = last_
  }

  final def +=(elem: A): Unit = {
    array(last_ % maxSize) = elem
    last_ += 1
    if (last_ > maxSize)
      first_ += 1
  }

  final def ++=(iterable: Iterable[A]) {
    for (elem <- iterable) this += elem
  }

  /** When the new data overlap with the old data, merge them both. If the resulting data is larger than the ringbuffer size, give preference to the new data. When the new data is not joined to the old data, the result is a RingBuffer with just the new data. */
  final def mergeWith(data: Seq[A], index: Int): Unit = {
    val newDataSize = data.size min maxSize
    if (index < first) {
      if (index + newDataSize < first) {
        first_ = index
        last_ = index + newDataSize
      } else {
        first_ = index
        last_ = ((first + newDataSize) max last) min (first + maxSize)
      }
    } else {
      if (index <= last) {
        last_ = (index + newDataSize) max last
        first_ = (index min first) max (last - maxSize)
      } else {
        first_ = index
        last_ = index + newDataSize
      }
    }
    0 until newDataSize foreach (i => array((index + i) % maxSize) = data(i))
  }
}
