package info.td.common.logging

import com.typesafe.scalalogging.Logger

import java.util.Date

object TraceHelpers {

  abstract class DumpFunction {
    def apply(s: => String): Unit
  }

  object printlnDumpFunction extends DumpFunction {
    def apply(s: => String): Unit = {
      println(s)
    }
  }

  def loggerErrorDumpFunction(logger: Logger): DumpFunction = new DumpFunction {
    def apply(s: => String): Unit = {
      logger.error(s)
    }
  }

  def loggerWarnDumpFunction(logger: Logger): DumpFunction = new DumpFunction {
    def apply(s: => String): Unit = {
      logger.warn(s)
    }
  }

  def loggerInfoDumpFunction(logger: Logger): DumpFunction = new DumpFunction {
    def apply(s: => String): Unit = {
      logger.info(s)
    }
  }

  def loggerDebugDumpFunction(logger: Logger): DumpFunction = new DumpFunction {
    def apply(s: => String): Unit = {
      logger.debug(s)
    }
  }

  def loggerTraceDumpFunction(logger: Logger): DumpFunction = new DumpFunction {
    def apply(s: => String): Unit = {
      logger.trace(s)
    }
  }

  object noOpDumpFunction extends DumpFunction {
    def apply(s: => String): Unit = {}
  }

  def traceDuration[T](name: String)(spec: => T)(implicit dumpFunction: DumpFunction = printlnDumpFunction): T = {
    val start = new Date().getTime
    dumpFunction(s"starting $name")
    val result = spec
    dumpFunction(s"finished $name in ${new Date().getTime - start} milliseconds")
    result
  }
}