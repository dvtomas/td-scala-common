package info.td.common.logging

import java.util.Date

import com.typesafe.scalalogging.Logger

object TraceHelpers {

  abstract class DumpFunction {
    def apply(s: => String)
  }

  object printlnDumpFunction extends DumpFunction {
    def apply(s: => String) {
      println(s)
    }
  }

  def loggerErrorDumpFunction(logger: Logger) = new DumpFunction {
    def apply(s: => String) {
      logger.error(s)
    }
  }

  def loggerWarnDumpFunction(logger: Logger) = new DumpFunction {
    def apply(s: => String) {
      logger.warn(s)
    }
  }

  def loggerInfoDumpFunction(logger: Logger) = new DumpFunction {
    def apply(s: => String) {
      logger.info(s)
    }
  }

  def loggerDebugDumpFunction(logger: Logger) = new DumpFunction {
    def apply(s: => String) {
      logger.debug(s)
    }
  }

  def loggerTraceDumpFunction(logger: Logger) = new DumpFunction {
    def apply(s: => String) {
      logger.trace(s)
    }
  }

  object noOpDumpFunction extends DumpFunction {
    def apply(s: => String) {}
  }

  def traceDuration[T](name: String)(spec: => T)(implicit dumpFunction: DumpFunction = printlnDumpFunction): T = {
    val start = new Date().getTime
    dumpFunction(s"starting $name")
    val result = spec
    dumpFunction(s"finished $name in ${new Date().getTime - start} milliseconds")
    result
  }
}