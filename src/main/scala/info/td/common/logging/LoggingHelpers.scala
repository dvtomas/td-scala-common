package info.td.common.logging

import ch.qos.logback.classic.{Level, Logger}
import org.slf4j.LoggerFactory

object LoggingHelpers {
  /* Overcome potential uninitialized logger that may appear due to initialization race condition: See
  * http://stackoverflow.com/questions/16000514/slf4j-logback-substitutelogger
  */
  def initializeLogging(): Unit = {
    LoggerFactory.getLogger("ROOT").asInstanceOf[Logger]
  }

  /** Purpose of this method is twofold:
   *
   * First, overcome potential uninitialized logger that may appear due to initialization race condition: See
   * http://stackoverflow.com/questions/16000514/slf4j-logback-substitutelogger
   *
   * Second, you can use it to manually override the default root logging level.
   */
  def initializeLogging(level: Level): Unit = {
    val root = LoggerFactory.getLogger("ROOT").asInstanceOf[Logger]
    root.setLevel(level)
  }
}