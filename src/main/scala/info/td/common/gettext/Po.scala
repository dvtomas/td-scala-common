package info.td.common.gettext

import com.typesafe.scalalogging.LazyLogging

import scala.annotation.tailrec
import java.text.MessageFormat
import java.util.Locale

// msgid_plural in po file is for translator.
// We do not need to store it here because it is
// available in the translation methods of class Po.
case class Translation(maybeContext: Option[String], singular: String, translation: Array[String])

class EmptyPo(autoEscapeSingleQuotes: Boolean) extends Po(Map.empty, autoEscapeSingleQuotes)

case class PoKey(maybeContext: Option[String], singular: String)

// body is a map of (context, singular) -> strings
class Po(val body: Map[PoKey, Array[String]], autoEscapeSingleQuotes: Boolean) extends LazyLogging {
  val locale: Locale = Locale.getDefault // TODO in the future it might be read from the .po file, or passed here by the PoLoader

  def ++(other: Po): Po = {
    // Note the order of "++", "other" will overwrite "this"
    val newBody = body ++ other.body

    // Ensure that pluralForms is not lost because "other" does not have it
    if (other.pluralForms0.isDefined) {
      new Po(newBody, autoEscapeSingleQuotes)
    } else {
      if (pluralForms0.isDefined) {
        val key = PoKey(None, "")
        val value = body(key)
        new Po(body + (key -> value), autoEscapeSingleQuotes)
      } else {
        new Po(newBody, autoEscapeSingleQuotes)
      }
    }
  }

  private def format(message: String, parameters: Array[Any]): String = {
    new MessageFormat(maybeEscapeSingleQuotes(message)) format parameters
  }

  def t(singular: String): String = lookupSingular(None, singular)

  def tf(singular: String, parameters: Any*): String = format(t(singular), Array(parameters: _*))

  def tc(ctx: String, singular: String): String = lookupSingular(Some(ctx), singular)

  def tcf(ctx: String, singular: String, parameters: Any*): String = format(lookupSingular(Some(ctx), singular), Array(parameters: _*))

  /** Only mark that string needs to be included in the GetText catalog, but don't actually translate it (yet) */
  def mark_t(singular: String): String = singular

  override def toString: String = {
    val buffer = new StringBuilder
    buffer.append("\n")
    body.foreach {
      case (PoKey(context, singular), strings) =>
        if (context.nonEmpty) {
          buffer.append(context.get)
          buffer.append("\n")
        }

        buffer.append(singular)
        buffer.append("\n")

        if (strings.length == 1) {
          buffer.append(strings(0))
          buffer.append("\n")
        } else {
          strings.zipWithIndex.foreach {
            case (str, index) =>
              buffer.append(index)
              buffer.append(" ")
              buffer.append(str)
              buffer.append("\n")
          }
        }
        buffer.append("\n")
    }
    buffer.toString()
  }

  private def maybeEscapeSingleQuotes(string: String) = if (autoEscapeSingleQuotes) {
    string flatMap {
      case '\'' => "''"
      case other => other.toString
    }
  } else {
    string
  }

  @tailrec
  private def lookupSingular(context: Option[String], singular: String): String = {
    if (singular.isEmpty) {
      // Must be here, because Po files contain Po meta information for the empty msgID
      ""
    } else {
      body.get(PoKey(context, singular)) match {
        case Some(strings) => strings(0)
        case None =>
          if (context.isDefined)
            lookupSingular(None, singular) // Try translation without context
          else
            singular
      }
    }
  }


  // See evaluatePluralForms below
  private val pluralForms0: Option[String] = {
    body.get(PoKey(None, "")) match {
      case None => None

      case Some(strings) =>
        val header = strings(0)
        header.linesIterator.find(_.startsWith("Plural-Forms")) match {
          case None => None

          case Some(line) =>
            Some(line.replace(" ", ""))
        }
    }
  }

  // This evaluate method can only work correctly with Plural-Forms exactly listed at:
  // http://www.gnu.org/software/gettext/manual/html_node/Plural-forms.html#Plural-forms
  // http://www.gnu.org/software/gettext/manual/html_node/Translating-plural-forms.html#Translating-plural-forms
  private val evaluatePluralForms: Long => Int = {
    if (pluralFormsMatched("nplurals=1; plural=0")) {
      n => 0
    } else if (pluralFormsMatched("Plural-Forms:nplurals=2;plural=(n!=1);")) {
      n => 0
    } else if (pluralFormsMatched("nplurals=2; plural=n != 1")) {
      n => if (n != 1) 1 else 0
    } else if (pluralFormsMatched("nplurals=2; plural=n>1")) {
      n => if (n > 1) 1 else 0
    } else if (pluralFormsMatched("nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2")) {
      n => if (n % 10 == 1 && n % 100 != 11) 0 else if (n != 0) 1 else 2
    } else if (pluralFormsMatched("nplurals=3; plural=n==1 ? 0 : n==2 ? 1 : 2")) {
      n => if (n == 1) 0 else if (n == 2) 1 else 2
    } else if (pluralFormsMatched("nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2")) {
      n => if (n == 1) 0 else if (n == 0 || (n % 100 > 0 && n % 100 < 20)) 1 else 2
    } else if (pluralFormsMatched("nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2")) {
      n => if (n % 10 == 1 && n % 100 != 11) 0 else if (n % 10 >= 2 && (n % 100 < 10 || n % 100 >= 20)) 1 else 2
    } else if (pluralFormsMatched("nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2")) {
      n => if (n % 10 == 1 && n % 100 != 11) 0 else if (n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20)) 1 else 2
    } else if (pluralFormsMatched("nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2")) {
      n => if (n == 1) 0 else if (n >= 2 && n <= 4) 1 else 2
    } else if (pluralFormsMatched("nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2")) {
      n => if (n == 1) 0 else if (n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20)) 1 else 2
    } else if (pluralFormsMatched("nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3")) {
      n => if (n % 100 == 1) 0 else if (n % 100 == 2) 1 else if (n % 100 == 3 || n % 100 == 4) 2 else 3
    } else {
      if (pluralForms0.isDefined) {
        logger.warn("Unknown plural form: " + pluralForms0)
      }
      n => 0
    }
  }

  private def pluralFormsMatched(pattern: String) = {
    pluralForms0 match {
      case None => false
      case Some(noSpaceLine) => noSpaceLine.indexOf(pattern.replace(" ", "")) >= 0
    }
  }
}
