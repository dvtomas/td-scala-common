package info.td.common.gettext

import java.io.File
import java.util.Locale

import com.typesafe.scalalogging.LazyLogging
import info.td.common.WithMessages
import info.td.common.file.FileUtils
import info.td.common.ui.UIMessage

import scala.util.parsing.combinator.JavaTokenParsers

object PoLoader extends LazyLogging {
  private val locale = Locale.getDefault

  case class PoDomain(domain: String)

  //  private val locale = new Locale("cs")

  def load(path: File, autoEscapeSingleQuotes: Boolean): WithMessages[Po] = {
    try {
      val contents = FileUtils.stringFromUTF8File(path).get
      PoParser.parsePo(contents, autoEscapeSingleQuotes) match {
        case Some(po) => WithMessages(po, Nil)
        case None => WithMessages(new EmptyPo(autoEscapeSingleQuotes), UIMessage.error(s"Error parsing translation file $path"))
      }
    } catch {
      case e: Throwable => WithMessages(new EmptyPo(autoEscapeSingleQuotes), UIMessage.error(s"Error loading translation file $path: $e"))
    }
  }

  private def pathOfTranslationFile(domain: PoDomain): File = new File(new File(new File("locale", locale.getLanguage), "LC_MESSAGES"), s"${domain.domain}.po")

  def load(domain: PoDomain, autoEscapeSingleQuotes: Boolean): WithMessages[Po] =
    load(pathOfTranslationFile(domain), autoEscapeSingleQuotes)
}

// http://www.gnu.org/software/hello/manual/gettext/PO-Files.html
object PoParser extends JavaTokenParsers {
  private def mergeStrings(quotedString: List[String]): String = {
    // Removes the first and last quote (") character of strings
    // and concatenates them
    val unquoted = quotedString.foldLeft("") { (acc, quoted) =>
      acc + quoted.substring(1, quoted.length - 1)
    }

    // Un-escape
    unquoted.
      replace("\\n", "\n").
      replace("\\r", "\r").
      replace("\\t", "\t").
      replace("\\\\", "\\")
  }

  /** Double quotes (`"`) enclosing a sequence of:
    *
    * - Any character except double quotes, control characters or backslash (`\`)
    * - A backslash followed by a slash, another backslash, or one of the letters
    * `b`, `f`, `n`, `r` or `t`.
    * - `\` followed by `u` followed by four hexadecimal digits
    */
  private val reStringLiteral: Parser[String] =
    ("\"" + """((\\\")|\p{Space}|\\u[a-fA-F0-9]{4}|[^"\p{Cntrl}\\]|\\[\\/bfnrt])*""" + "\"").r

  // Scala regex is single line by default
  private def comment = rep(regex("^#.*".r))

  private def msgContext = "msgctxt" ~ rep(reStringLiteral) ^^ {
    case _ ~ quotedString => mergeStrings(quotedString)
  }

  private def msgId = "msgid" ~ rep(reStringLiteral) ^^ {
    case _ ~ quotedString => mergeStrings(quotedString)
  }

  private def msgIdPlural = "msgid_plural" ~ rep(reStringLiteral) ^^ {
    case _ ~ quotedString => mergeStrings(quotedString)
  }

  private def msgString = "msgstr" ~ rep(reStringLiteral) ^^ {
    case _ ~ quotedString => mergeStrings(quotedString)
  }

  private def msgStringN = "msgstr[" ~ wholeNumber ~ "]" ~ rep(reStringLiteral) ^^ {
    case _ ~ number ~ _ ~ quotedString => (number.toInt, mergeStrings(quotedString))
  }

  private def singular =
    (opt(comment) ~ opt(msgContext) ~
      opt(comment) ~ msgId ~
      opt(comment) ~ msgString ~ opt(comment)) ^^ {
      case _ ~ ctxo ~ _ ~ id ~ _ ~ str ~ _ =>
        Translation(ctxo, id, Array(str))
    }

  private def plural =
    (opt(comment) ~ opt(msgContext) ~
      opt(comment) ~ msgId ~
      opt(comment) ~ msgIdPlural ~
      opt(comment) ~ rep(msgStringN) ~ opt(comment)) ^^ {
      case _ ~ ctxo ~ _ ~ id ~ _ ~ _ ~ _ ~ n_strs ~ _ =>
        val strs = n_strs.sorted.map { case (_, str) => str }
        Translation(ctxo, id, strs.toArray)
    }

  private def exp = rep(singular | plural)

  def parsePo(po: String, autoEscapeSingleQuotes: Boolean): Option[Po] = {
    val parseRet = parseAll(exp, po)
    if (parseRet.successful) {
      val translations = parseRet.get
      val body = translations.foldLeft(
        Map[PoKey, Array[String]]()) { (acc, t) =>
        if (t.translation.forall(_.isEmpty)) {
          acc
        } else {
          acc + (PoKey(t.maybeContext, t.singular) -> t.translation)
        }
      }
      Some(new Po(body, autoEscapeSingleQuotes))
    } else {
      None
    }
  }
}
