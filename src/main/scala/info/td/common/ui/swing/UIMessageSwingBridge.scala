package info.td.common.ui.swing

import java.awt.event.{MouseAdapter, MouseEvent, MouseListener}
import java.awt.font.TextAttribute
import java.awt.{Color, Cursor}
import java.util
import javax.swing._
import javax.swing.text.JTextComponent

import info.td.common.exceptions.UserError
import info.td.common.rx.RxHelpers
import info.td.common.rx.RxHelpers._
import info.td.common.ui.UIElements._
import info.td.common.ui.UIMessage
import net.miginfocom.swing.MigLayout
import rx.lang.scala.{Subscription, Observable, Subject}

object UIMessageSwingBridge {
  private[swing] val warningColor = new Color(240, 240, 160)
  private[swing] val errorColor = new Color(240, 160, 160)

  lazy val font = new JTextField().getFont

  /** A single-line panel with the message. */
  def textField: UIMessageSwingBridge = {
    val textComponent = new JTextField("|")
    new UIMessageSwingBridge(textComponent, textComponent)
  }

  def textFieldWithStaticString(string: String): UIMessageSwingBridge = {
    val textComponent = new JTextField("|")
    val field = new UIMessageSwingBridge(textComponent, textComponent)
    field.setMessage(UIMessage.info(string))
    field
  }

  def textFieldWithStaticMessage(message: UIMessage): UIMessageSwingBridge = {
    val textComponent = new JTextField("|")
    val field = new UIMessageSwingBridge(textComponent, textComponent)
    field.setMessage(message)
    field
  }

  /** A multi-line panel with the message */
  def textArea: UIMessageSwingBridge = {
    val textComponent = new JTextArea("|")
    textComponent.setFont(font)
    textComponent.setLineWrap(true)
    textComponent.setWrapStyleWord(true)
    val scrollPane = new JScrollPane(textComponent)
    new UIMessageSwingBridge(scrollPane, textComponent)
  }

  /** A multi-line panel with the message */
  def textAreaWithStaticString(string: String): UIMessageSwingBridge = {
    val area = textArea
    area.setMessage(UIMessage.info(string))
    area
  }
}

class UIMessageSwingBridge private(val component: JComponent, val textComponent: JTextComponent) extends UIMessageLabel {
  textComponent.setEditable(false)
  textComponent.setBorder(null)

  private var maybeClickMouseListener: Option[MouseListener] = None

  private val originalCursor = textComponent.getCursor

  private[this] val currentMessageSubject = Subject[UIMessage]()

  def currentMessageSource: Observable[UIMessage] = currentMessageSubject

  private[this] var maybeCurrentMessage: Option[UIMessage] = None

  def makeLabelHyperlink(clickAction: UIMessage => Unit) {
    SwingHelpers.invokeOnEDT {
      textComponent.setCursor(new Cursor(Cursor.HAND_CURSOR))
      val font = textComponent.getFont
      val attributes: util.Map[TextAttribute, Object] = font.getAttributes.asInstanceOf[util.Map[TextAttribute, Object]]
      attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON)
      textComponent.setFont(font.deriveFont(attributes))

      val clickMouseListener = new MouseAdapter() {
        override def mouseClicked(event: MouseEvent) {
          maybeCurrentMessage foreach (currentMessage => clickAction(currentMessage))
        }
      }
      maybeClickMouseListener = Some(clickMouseListener)
      textComponent.addMouseListener(clickMouseListener)
    }
  }

  def makeLabelPlain() {
    SwingHelpers.invokeOnEDT {
      maybeClickMouseListener foreach textComponent.removeMouseListener
      maybeClickMouseListener = None
      textComponent.setCursor(originalCursor)
      val font = textComponent.getFont
      val attributes: util.Map[TextAttribute, Object] = font.getAttributes.asInstanceOf[util.Map[TextAttribute, Object]]
      attributes.remove(TextAttribute.UNDERLINE)
      textComponent.setFont(font.deriveFont(attributes))
    }
  }

  protected[swing] def setMessage(message: UIMessage) {
    maybeCurrentMessage = Some(message)
    currentMessageSubject onNext message
    textComponent setBackground (message.messageType match {
      case UIMessage.Info => null
      case UIMessage.Warning => UIMessageSwingBridge.warningColor
      case UIMessage.Error(_) => UIMessageSwingBridge.errorColor
    })
    textComponent.setText(message.string)
    textComponent.setCaretPosition(0)
  }

  override protected[this] def setString(string: String) {
    setMessage(UIMessage.info(string))
  }

  override def subscribeToMessages(messageSource: Observable[UIMessage])(implicit schedulerProvider: RxHelpers.SchedulerProvider): Subscription = {
    super.subscribeToMessages(messageSource.doOnCompleted {
      maybeClickMouseListener foreach textComponent.removeMouseListener
      currentMessageSubject.onCompleted()
    })
  }
}

object UIMessagePanelSwingBridge {
  /**
   * A big panel, that just displays information and warnings, but for errors it displays details of the error that has occurred.
   */
  def messagePanel: UIMessagePanelSwingBridge = new UIMessagePanelSwingBridge()

  def messagePanelWithMessage(message: UIMessage)(implicit schedulerProvider: SchedulerProvider): JPanel = {
    val messagePanel = UIMessagePanelSwingBridge.messagePanel
    messagePanel.subscribeToMessages(Observable.just(message))
    messagePanel.component
  }
}

class UIMessagePanelSwingBridge private() extends UIMessageLabel {
  val component: JPanel = new JPanel()

  protected[this] def setMessage(message: UIMessage) = {
    component.removeAll()
    message.messageType match {
      case UIMessage.Error(Some(error)) => {
        component.setLayout(new MigLayout("insets 0, fill", "[fill]", "[grow, fill]"))
        val panelWithError = new SwingPanelWithError(new UserError(message.string, error))
        panelWithError.textPane.setBackground(UIMessageSwingBridge.errorColor)
        component.add(panelWithError) // Prepend the message string in an UserError
      }
      case _ => {
        val messageLabel = UIMessageSwingBridge.textArea
        messageLabel.setMessage(message)
        component.setLayout(new MigLayout("insets 0, fill", "[fill]", "[fill]"))
        component.add(messageLabel.component)
      }
    }
    // both validate and revalidate seem to be necessary
    component.validate()
    component.revalidate()
    component.repaint()
  }

  protected[this] def setString(string: String) = setMessage(UIMessage.info(string))
}