package info.td.common.ui.swing

import java.awt._
import javax.swing.JLabel

object ScaledImagePanel

/**
 * The image is a by name for performance reason, imagine this panel being on a tab that is hidden most of the time, and having setImageData called rapidly.
 *
 * However, when you want a custom function to generate the image, you will most certainly need to override getPreferredSize to NOT use its dimensions, otherwise it will evaluate too early on revalidate.
 **/
class ScaledImagePanel(
  _image: => Image, // If you want
  renderingHints: RenderingHints = new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED)
) extends JLabel {
  lazy val image: Image = _image

  override def paintComponent(g: Graphics) {
    val g2d: Graphics2D = g.asInstanceOf[Graphics2D]
    g2d.addRenderingHints(renderingHints)
    g2d.drawImage(image, 0, 0, getWidth, getHeight, Color.BLACK, null)
  }

  protected def imageDimensions = new Dimension(image.getWidth(null), image.getHeight(null))

  override def getPreferredSize: Dimension = {
    Option(getParent) match {
      case None => imageDimensions
      case Some(parentContainer) => {
        if (imageDimensions.width == 0 || parentContainer.getWidth == 0)
          imageDimensions
        else {
          val imageRatio = imageDimensions.getWidth / imageDimensions.getHeight
          val containerRatio = parentContainer.getWidth / parentContainer.getHeight
          if (imageRatio > containerRatio) {
            new Dimension(parentContainer.getWidth, (parentContainer.getWidth / imageRatio).toInt)
          } else
            new Dimension((parentContainer.getHeight * imageRatio).toInt, parentContainer.getHeight)
        }
      }
    }
  }
}
