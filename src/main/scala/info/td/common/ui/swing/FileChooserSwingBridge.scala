package info.td.common.ui.swing

import java.awt.event.{ActionEvent, ActionListener}
import java.io.File
import javax.swing.{AbstractButton, JButton, JFileChooser}

import info.td.common.ui.UIElements.FileChooser
import info.td.common.ui.swing.FileChooserSwingBridge.FileChooserAction
import rx.lang.scala.Observable

object FileChooserSwingBridge {

  abstract class FileChooserAction {
    def beforeOpenAction(chooser: JFileChooser) {}

    def afterOpenAction(chooser: JFileChooser) {}
  }

  class RememberLastDirectoryAction extends FileChooserAction {
    private[this] var maybeInitialDirectory: Option[File] = Some(new File("."))

    override def beforeOpenAction(chooser: JFileChooser): Unit = {
      maybeInitialDirectory foreach chooser.setCurrentDirectory
    }

    override def afterOpenAction(chooser: JFileChooser): Unit = {
      maybeInitialDirectory = Some(
        if (chooser.isDirectorySelectionEnabled && !chooser.isFileSelectionEnabled)
          chooser.getSelectedFile.getParentFile // We are selecting directories only. We are not interested in remembering the last selected directory, but its parent, so we can choose selected directory's sibling next time.
        else
          chooser.getSelectedFile
      )
    }
  }

  class SetInitialDirectoryAction extends FileChooserAction {
    private[this] var maybeInitialDirectory: Option[File] = Some(new File("."))

    def subscribeInitialDirectoryTo(initialDirectorySource: Observable[File]): Unit = {
      initialDirectorySource.subscribe(directory => maybeInitialDirectory = Some(directory))
    }

    override def beforeOpenAction(chooser: JFileChooser): Unit = {
      maybeInitialDirectory foreach chooser.setCurrentDirectory
    }

    override def afterOpenAction(chooser: JFileChooser) {}
  }

  object SetMultiSelectionAction extends FileChooserAction {
    override def beforeOpenAction(chooser: JFileChooser): Unit = {
      chooser setMultiSelectionEnabled true
    }
  }

}

class FileChooserSwingBridge private(val component: AbstractButton, selectDialogTitle: String, fileSelectionMode: Int, chooserActions: Seq[FileChooserAction]) extends FileChooser {
  def this(buttonTitle: String, selectDialogTitle: String, fileSelectionMode: Int) = this(new JButton(buttonTitle), selectDialogTitle, fileSelectionMode: Int, Seq.empty)

  def this(buttonTitle: String, selectDialogTitle: String, fileSelectionMode: Int, chooserActions: Seq[FileChooserAction]) = this(new JButton(buttonTitle), selectDialogTitle, fileSelectionMode: Int, chooserActions)

  def setEnabled(enabled: Boolean): Unit = component.setEnabled(enabled)

  lazy val fileSource: Observable[Seq[File]] = Observable[Seq[File]](subscriber => {
    val listener = new ActionListener {
      def actionPerformed(e: ActionEvent): Unit = {
        val chooser = new JFileChooser()
        chooser.setDialogTitle(selectDialogTitle)
        chooser.setFileSelectionMode(fileSelectionMode)
        chooser.setAcceptAllFileFilterUsed(false)
        chooserActions foreach (_.beforeOpenAction(chooser))
        if (JFileChooser.APPROVE_OPTION == chooser.showOpenDialog(null)) {
          chooserActions foreach (_.afterOpenAction(chooser))
          val files = if (chooser.isMultiSelectionEnabled)
            chooser.getSelectedFiles.toSeq
          else
            Seq(chooser.getSelectedFile)
          subscriber onNext files
        }
      }
    }

    component.addActionListener(listener)
    subscriber.add(component.removeActionListener(listener))
  })
}