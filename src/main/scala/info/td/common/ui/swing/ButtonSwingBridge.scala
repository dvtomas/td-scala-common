package info.td.common.ui.swing

import java.awt.event.{ActionEvent, ActionListener}
import javax.swing.{AbstractButton, Icon, ImageIcon, JButton}

import info.td.common.ui.UIElements.Button
import rx.lang.scala.{Observable, Subscriber}

class ButtonSwingBridge protected(_component: AbstractButton, private var text: String, maybeIcon: Option[Icon]) extends Button {
  def this(component: AbstractButton) = this(component, component.getText, Option(component.getIcon))

  def this(text: String) = this(new JButton(text), text, None)

  def this(text: String, icon: ImageIcon) = this(new JButton(text), text, Some(icon))

  def component: AbstractButton = _component

  def setEnabled(enabled: Boolean): Unit = component.setEnabled(enabled)

  lazy val clicks: Observable[Unit] =
    Observable((subscriber: Subscriber[Unit]) => {
      val listener = new ActionListener {
        def actionPerformed(e: ActionEvent): Unit = {
          subscriber.onNext(())
        }
      }
      component.addActionListener(listener)
      subscriber.add(component removeActionListener listener)
    })

  private var displayText = true

  def setText(newText: String): Unit = {
    text = newText
    if (displayText)
      component.setText(text)
  }

  def setIcon(imageResourcePath: String): Unit = {
    val newIcon = SwingHelpers.imageIcon(imageResourcePath)
    if (component.getIcon != null)
      component.setIcon(newIcon)
  }

  def displayTextOnly(): Unit = {
    displayText = true
    component.setText(text)
    component.setIcon(null)
  }

  def displayIconAndText(): Unit = {
    displayText = true
    component.setText(text)
    maybeIcon foreach component.setIcon
  }

  def displayIconOnly(): Unit = {
    displayText = false
    component.setText("")
    maybeIcon foreach component.setIcon
  }
}