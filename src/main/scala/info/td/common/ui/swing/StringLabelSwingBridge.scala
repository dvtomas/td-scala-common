package info.td.common.ui.swing

import javax.swing.JLabel

import info.td.common.ui.UIElements.StringLabel

class StringLabelSwingBridge private(val component: JLabel) extends StringLabel {
  def this() = this(new JLabel("|"))

  override def setString(string: String): Unit = {
    component.setText(string)
  }
}