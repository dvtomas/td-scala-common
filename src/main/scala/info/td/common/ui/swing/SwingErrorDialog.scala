package info.td.common.ui.swing

import java.awt.datatransfer.StringSelection
import java.awt.event.ActionEvent
import java.awt.{Dimension, Frame, Toolkit}

import com.typesafe.scalalogging.LazyLogging
import info.td.common._
import info.td.common.exceptions.ExceptionHelpers._
import info.td.common.exceptions.UserError
import javax.swing._
import javax.swing.border.EtchedBorder
import javax.swing.text.{DefaultStyledDocument, StyleConstants, StyleContext}
import net.miginfocom.swing.MigLayout

class SwingErrorDialog(error: Throwable) extends JFrame with JFrameHelpers with LazyLogging {
  def open(): Unit = SwingHelpers.invokeOnEDT {
    val iconResource = getClass.getResource("/images/exclamation.ico")
    val iconImage = new ImageIcon(iconResource).getImage
    setIconImage(iconImage)
    setTitle(i18n.tc("errorDialog", "An error has occurred"))
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)
    closeOnEscape()
    setState(Frame.NORMAL)
    setVisible(true)
  }

  private def create() {
    val mainPanel = getContentPane
    mainPanel.setLayout(new MigLayout("", "[grow,fill]", "[grow,fill][]"))
    val panelWithError = new SwingPanelWithError(error)
    panelWithError.setBorder(new EtchedBorder())
    mainPanel.add(panelWithError, "wrap")
    mainPanel.add(buttonClose, "growx")

    getRootPane.setDefaultButton(buttonClose)
    pack()
  }

  create()
}

class SwingPanelWithError(error: Throwable) extends JPanel with LazyLogging {
  private[this] def defaultDocument = {
    val doc = new DefaultStyledDocument()
    val base = StyleContext.getDefaultStyleContext.getStyle(StyleContext.DEFAULT_STYLE)
    val emphasized = doc.addStyle("emphasized", base)
    StyleConstants.setBold(emphasized, true)
    val big = doc.addStyle("big", base)
    StyleConstants.setBold(big, true)
    StyleConstants.setFontSize(big, (StyleConstants.getFontSize(big) * 1.2).toInt)
    doc
  }

  private[this] def noDetailsDocument = {
    val doc = defaultDocument
    val big = doc.getStyle("big")
    if (!error.causalList.exists(_.isInstanceOf[UserError])) {
      logger.warn("no UserError in chain :", error.toString)
    }
    for (e <- error.causalList if e.isInstanceOf[UserError]) {
      doc.insertString(doc.getLength, e.getLocalizedMessage + "\n", big)
    }
    doc
  }

  private[this] def detailsDocument = {
    val doc = defaultDocument
    val big = doc.getStyle("big")
    val emphasized = doc.getStyle("emphasized")
    for (e <- error.causalList) {
      doc.insertString(doc.getLength, e.getLocalizedMessage + "\n", if (e.isUserError) big else emphasized)
    }
    doc
  }

  private[this] def allDetailsDocument = {
    val doc = defaultDocument
    doc.insertString(doc.getLength, error.stackTraceString, StyleContext.getDefaultStyleContext.getStyle(StyleContext.DEFAULT_STYLE))
    doc
  }

  val textPane = new JTextPane(noDetailsDocument)

  private def create() {

    textPane.setEditable(false)

    val textScrollPane = new JScrollPane(textPane)
    textScrollPane.setPreferredSize(new Dimension(600, 370 / 2))

    def detailsRadioButton(label: String, document: => DefaultStyledDocument) = {
      val button = new JRadioButton(label)
      button.addActionListener {
        _: ActionEvent => {
          textPane.setDocument(document)
        }
      }
      button
    }

    val buttonNoDetails = detailsRadioButton(i18n.tc("errorDialog", "No details"), noDetailsDocument)
    val buttonDetails = detailsRadioButton(i18n.tc("errorDialog", "Details"), detailsDocument)
    val buttonAllDetails = detailsRadioButton(i18n.tc("errorDialog", "All details"), allDetailsDocument)

    val detailsGroup = new ButtonGroup
    detailsGroup.add(buttonNoDetails)
    detailsGroup.add(buttonDetails)
    detailsGroup.add(buttonAllDetails)
    buttonNoDetails.setSelected(true)

    val buttonCopyToClipboard = new JButton(i18n.tc("errorDialog", "Copy to clipboard"))

    buttonCopyToClipboard.addActionListener {
      _: ActionEvent => {
        val c = Toolkit.getDefaultToolkit.getSystemClipboard
        val data = new StringSelection(error.stackTraceString)
        c.setContents(data, data)
      }
    }

    setLayout(new MigLayout("insets 0, fill", "[][][][grow]", "[grow][]"))
    add(textScrollPane, "span, growx, growy, wrap")
    add(buttonNoDetails)
    add(buttonDetails)
    add(buttonAllDetails)
    add(buttonCopyToClipboard, "alignx right")
  }

  create()
}

object SwingErrorDialogTest {
  def main(args: Array[String]): Unit = {
    new SwingErrorDialog(UserError("An error in the cause list", new RuntimeException("The original cause"))).open()
  }
}