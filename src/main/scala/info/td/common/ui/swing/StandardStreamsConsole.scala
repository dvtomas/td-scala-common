package info.td.common.ui.swing

import java.awt._
import java.awt.datatransfer.StringSelection
import java.awt.event.ActionEvent
import java.io._

import info.td.common._
import info.td.common.collection.mutable.RingBuffer
import javax.swing._
import javax.swing.event._
import javax.swing.text._
import net.miginfocom.swing.MigLayout

private[this] class LimitLinesDocumentListener(maximumLines: Int) extends DocumentListener {

  def insertUpdate(e: DocumentEvent) {
    SwingHelpers.invokeLater(removeLines(e))
  }

  def removeUpdate(e: DocumentEvent) {}

  def changedUpdate(e: DocumentEvent) {}

  private[this] def removeLines(e: DocumentEvent) {
    val document = e.getDocument
    val root = document.getDefaultRootElement
    while (root.getElementCount > maximumLines) {
      removeFromStart(document, root)
    }
  }

  private[this] def removeFromStart(document: Document, root: Element) {
    val line = root.getElement(0)
    val end = line.getEndOffset
    try {
      document.remove(0, end)
    }
    catch {
      case exception: BadLocationException => println(exception)
    }
  }
}

class StandardStreamsConsole(linesLimit: Int) {
  private[this] var maybeTextComponent: Option[JTextComponent] = None
  private[this] var redirectedStreams: Seq[ConsoleOutputStream] = Seq.empty

  def setTextComponent(textComponent: JTextComponent): Unit = {
    val linesListener = new LimitLinesDocumentListener(linesLimit)

    SwingHelpers.invokeLater {
      textComponent.setEditable(false)
      val document = textComponent.getDocument
      document.addDocumentListener(linesListener)
      maybeTextComponent = Some(textComponent)
      redirectedStreams foreach (_.flushBuffer(textComponent))
    }
  }

  def redirectOut(textColor: Color = Color.BLACK) {
    val consoleOutputStream = new ConsoleOutputStream(textColor)
    System.setOut(new PrintStream(consoleOutputStream, true))
    redirectedStreams +:= consoleOutputStream
  }

  def redirectErr(textColor: Color = Color.RED) {
    val consoleOutputStream = new ConsoleOutputStream(textColor)
    System.setErr(new PrintStream(consoleOutputStream, true))
    redirectedStreams +:= consoleOutputStream
  }

  class ConsoleOutputStream(textColor: Color) extends ByteArrayOutputStream {
    private var attributes: SimpleAttributeSet = null
    private val buffer = new RingBuffer[String](linesLimit)

    if (textColor != null) {
      attributes = new SimpleAttributeSet
      StyleConstants.setForeground(attributes, textColor)
    }

    /*
 *  Override this method to intercept the output text. Each line of text
 *  output will actually involve invoking this method twice:
 *
 *  a) for the actual text message
 *  b) for the newLine string
 *
 *  The message will be treated differently depending on whether the line
 *  will be appended or inserted into the Document
 */
    override def flush() {
      val message: String = toString
      if (message.nonEmpty) {
        handleAppend(message)
      }
      reset()
    }

    private[this] def handleAppend(message: String) {
      buffer += message
      maybeTextComponent foreach (textComponent => flushBuffer(textComponent))
    }

    private[StandardStreamsConsole] def flushBuffer(textComponent: JTextComponent) {
      (buffer.first until buffer.last) map buffer.apply foreach { line =>
        try {
          val document = textComponent.getDocument
          val offset: Int = document.getLength
          document.insertString(offset, line, attributes)
          textComponent.setCaretPosition(document.getLength)
        }
        catch {
          case _: BadLocationException => {
          }
        }
      }
      buffer.clear()
    }
  }

}

class ConsolePanel(console: StandardStreamsConsole) {
  def title = "Standard out/error output console"

  val panel: JPanel = {
    val p = new JPanel
    val textPane = new JTextPane
    val scrollPane = new JScrollPane(textPane)
    p.setLayout(new MigLayout("fill, wrap", "[grow, fill]", "[fill][grow, fill]"))
    p.add(new JLabel(title))
    p.add(scrollPane)
    console setTextComponent textPane

    val popupMenu = new JPopupMenu()
    val menuItem = new JMenuItem(i18n.t("Copy to clipboard"))
    menuItem.addActionListener((_: ActionEvent) => {
      val selectedText = Option(textPane.getSelectedText).getOrElse("")
      val text = if (selectedText.isEmpty) {
        textPane.getText
      } else {
        selectedText
      }
      val c = Toolkit.getDefaultToolkit.getSystemClipboard
      val data = new StringSelection(text)
      c.setContents(data, data)
    })
    popupMenu.add(menuItem)
    SwingHelpers.addPopupListener(textPane, popupMenu)

    p
  }
}

class ConsoleFrame(console: StandardStreamsConsole) {
  private[this] lazy val frame: JFrame = {
    val f = new JFrame()
    val panel = f.getContentPane
    panel.setLayout(new MigLayout("fill, insets 0", "[fill, grow]", "[fill, grow]"))
    val consolePanel = new ConsolePanel(console)
    panel.add(consolePanel.panel)

    f.setTitle(consolePanel.title)
    f.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE)
    f.setMinimumSize(new Dimension(320, 200))
    f.pack()
    f
  }

  def openOrRaise(): Unit = {
    frame.setState(Frame.NORMAL)
    frame.setVisible(true)
  }
}
