package info.td.common.ui.swing

import java.awt._
import javax.swing.{Icon, JLabel, JComponent}
import javax.swing.plaf.basic.BasicLabelUI

class VerticalLabelUI(clockwise: Boolean) extends BasicLabelUI {
  private[this] var verticalViewR: Rectangle = new Rectangle
  private[this] var verticalIconR: Rectangle = new Rectangle
  private[this] var verticalTextR: Rectangle = new Rectangle

  /**
   * Overridden to always return -1, since a vertical label does not have a
   * meaningful baseline.
   *
   * @see ComponentUI#getBaseline(JComponent, int, int)
   */
  override def getBaseline(c: JComponent, width: Int, height: Int): Int = -1

  /**
   * Overridden to always return Component.BaselineResizeBehavior.OTHER,
   * since a vertical label does not have a meaningful baseline
   *
   * @see ComponentUI#getBaselineResizeBehavior(javax.swing.JComponent)
   */
  override def getBaselineResizeBehavior(c: JComponent): Component.BaselineResizeBehavior = Component.BaselineResizeBehavior.OTHER

  /**
   * Transposes the view rectangles as appropriate for a vertical view
   * before invoking the super method and copies them after they have been
   * altered by {@link SwingUtilities#layoutCompoundLabel(FontMetrics, String,
    * Icon, int, int, int, int, Rectangle, Rectangle, Rectangle, int)}
   */
  protected override def layoutCL(label: JLabel, fontMetrics: FontMetrics, text: String, icon: Icon, viewR: Rectangle, iconR: Rectangle, textR: Rectangle): String = {
    verticalViewR = transposeRectangle(viewR)
    verticalIconR = transposeRectangle(iconR)
    verticalTextR = transposeRectangle(textR)
    val newText = super.layoutCL(label, fontMetrics, text, icon, verticalViewR, verticalIconR, verticalTextR)
    copyRectangle(verticalViewR, viewR)
    copyRectangle(verticalIconR, iconR)
    copyRectangle(verticalTextR, textR)
    newText
  }

  override def paint(g: Graphics, c: JComponent) {
    val g2: Graphics2D = g.create.asInstanceOf[Graphics2D]
    if (clockwise) {
      g2.rotate(Math.PI / 2, c.getWidth / 2, c.getWidth / 2)
    }
    else {
      g2.rotate(-Math.PI / 2, c.getHeight / 2, c.getHeight / 2)
    }
    super.paint(g2, c)
  }

  override def getPreferredSize(c: JComponent): Dimension = transposeDimension(super.getPreferredSize(c))

  override def getMaximumSize(c: JComponent): Dimension = transposeDimension(super.getPreferredSize(c))

  override def getMinimumSize(c: JComponent): Dimension = transposeDimension(super.getPreferredSize(c))

  private def transposeDimension(dimension: Dimension): Dimension = new Dimension(dimension.height, dimension.width + 2)

  private def transposeRectangle(rectangle: Rectangle): Rectangle = new Rectangle(rectangle.y, rectangle.x, rectangle.height, rectangle.width)

  private def copyRectangle(from: Rectangle, to: Rectangle) {
    Option(to).foreach(rectangle => {
      rectangle.x = from.x
      rectangle.y = from.y
      rectangle.width = from.width
      rectangle.height = from.height
    })
  }
}
