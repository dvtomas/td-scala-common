package info.td.common.ui.swing

import java.awt._
import java.awt.event._
import java.beans._
import javax.swing._
import javax.swing.event._
import javax.swing.table._

/**
 * This is a port of http://tips4java.wordpress.com/2008/11/10/table-column-adjuster/
 *
 * It dynamically adjusts column widths according to the contents of the table. See the link for more details.
 *
 * @param table Table to adjust
 * @param isDynamicAdjustment Hook on table change events, and dynamically change the widths when the contents changes
 * @param isOnlyAdjustLarger Only make the columns grow, never shrink
 * @param isColumnHeaderIncluded Include column headers in the width computation
 * @param isColumnDataIncluded Include column data in the width computation
 * @param spacing extra space for the column width
 */
class TableColumnAdjuster(table: JTable, isDynamicAdjustment: Boolean, isOnlyAdjustLarger: Boolean, isColumnHeaderIncluded: Boolean, isColumnDataIncluded: Boolean, spacing: Int = 6) extends PropertyChangeListener with TableModelListener {
  private var columnSizes = Map[TableColumn, Integer]()

  if (isDynamicAdjustment) {
    table.addPropertyChangeListener(this)
    table.getModel.addTableModelListener(this)
  }
  else {
    table.removePropertyChangeListener(this)
    table.getModel.removeTableModelListener(this)
  }

  def adjustColumns() {
    (0 until table.getColumnModel.getColumnCount) foreach adjustColumn
  }

  def adjustColumn(column: Int) {
    val tableColumn: TableColumn = table.getColumnModel.getColumn(column)
    if (tableColumn.getResizable) {
      val columnHeaderWidth: Int = getColumnHeaderWidth(column)
      val columnDataWidth: Int = getColumnDataWidth(column)
      val preferredWidth: Int = Math.max(columnHeaderWidth, columnDataWidth)
      updateTableColumn(column, preferredWidth)
    }
  }

  private def getColumnHeaderWidth(column: Int): Int = {
    if (isColumnHeaderIncluded) {
      val tableColumn: TableColumn = table.getColumnModel.getColumn(column)
      val value: AnyRef = tableColumn.getHeaderValue
      var renderer: TableCellRenderer = tableColumn.getHeaderRenderer
      if (renderer == null) {
        renderer = table.getTableHeader.getDefaultRenderer
      }
      val c: Component = renderer.getTableCellRendererComponent(table, value, false, false, -1, column)
      c.getPreferredSize.width
    } else {
      0
    }
  }

  private def getColumnDataWidth(column: Int): Int = {
    if (!isColumnDataIncluded) return 0
    var preferredWidth: Int = 0
    val maxWidth: Int = table.getColumnModel.getColumn(column).getMaxWidth
    (0 until table.getRowCount) foreach { row =>
      preferredWidth = Math.max(preferredWidth, getCellDataWidth(row, column))
      if (preferredWidth >= maxWidth) return maxWidth
    }
    preferredWidth
  }

  private def getCellDataWidth(row: Int, column: Int): Int = {
    val cellRenderer: TableCellRenderer = table.getCellRenderer(row, column)
    val c: Component = table.prepareRenderer(cellRenderer, row, column)
    val width: Int = c.getPreferredSize.width + table.getIntercellSpacing.width
    width
  }

  private def updateTableColumn(column: Int, width: Int) {
    val tableColumn: TableColumn = table.getColumnModel.getColumn(column)
    if (tableColumn.getResizable) {
      columnSizes += tableColumn -> new Integer(tableColumn.getWidth)
      table.getTableHeader.setResizingColumn(tableColumn)
      if (isOnlyAdjustLarger) {
        tableColumn.setWidth(Math.max(width + spacing, tableColumn.getPreferredWidth))
      } else {
        tableColumn.setWidth(width + spacing)
      }
    }
  }

  def restoreColumns() {
    (0 until table.getColumnModel.getColumnCount) foreach restoreColumn
  }

  private def restoreColumn(column: Int) {
    val tableColumn: TableColumn = table.getColumnModel.getColumn(column)
    val width: Integer = columnSizes(tableColumn)
    if (width != null) {
      table.getTableHeader.setResizingColumn(tableColumn)
      tableColumn.setWidth(width.intValue)
    }
  }

  def propertyChange(e: PropertyChangeEvent) {
    if ("model" == e.getPropertyName) {
      var model: TableModel = e.getOldValue.asInstanceOf[TableModel]
      model.removeTableModelListener(this)
      model = e.getNewValue.asInstanceOf[TableModel]
      model.addTableModelListener(this)
      adjustColumns()
    }
  }

  def tableChanged(e: TableModelEvent) {
    if (!isColumnDataIncluded) return
    if (e.getType == TableModelEvent.UPDATE) {
      val column: Int = table.convertColumnIndexToView(e.getColumn)
      if (isOnlyAdjustLarger) {
        val row: Int = e.getFirstRow
        val tableColumn: TableColumn = table.getColumnModel.getColumn(column)
        if (tableColumn.getResizable) {
          val width: Int = getCellDataWidth(row, column)
          updateTableColumn(column, width)
        }
      }
      else {
        adjustColumn(column)
      }
    }
    else {
      adjustColumns()
    }
  }

  class ColumnAction(isSelectedColumn: Boolean, isAdjust: Boolean) extends AbstractAction {
    def actionPerformed(e: ActionEvent) {
      if (isSelectedColumn) {
        table.getSelectedColumns foreach { column =>
          if (isAdjust) {
            adjustColumn(column)
          } else {
            restoreColumn(column)
          }
        }
      } else {
        if (isAdjust) {
          adjustColumns()
        } else {
          restoreColumns()
        }
      }
    }
  }

  /** Use this to install keyboard actions. See http://tips4java.wordpress.com/2008/11/10/table-column-adjuster/ for details */
  def installActions() {
    installColumnAction(isSelectedColumn = true, isAdjust = true, "adjustColumn", "control ADD")
    installColumnAction(isSelectedColumn = false, isAdjust = true, "adjustColumns", "control shift ADD")
    installColumnAction(isSelectedColumn = true, isAdjust = false, "restoreColumn", "control SUBTRACT")
    installColumnAction(isSelectedColumn = false, isAdjust = false, "restoreColumns", "control shift SUBTRACT")
  }

  private def installColumnAction(isSelectedColumn: Boolean, isAdjust: Boolean, key: String, keyStroke: String) {
    val action: Action = new ColumnAction(isSelectedColumn, isAdjust)
    val ks: KeyStroke = KeyStroke.getKeyStroke(keyStroke)
    table.getInputMap.put(ks, key)
    table.getActionMap.put(key, action)
  }
}
