package info.td.common.ui.swing

import java.awt.{BorderLayout, Dimension}
import javax.swing._

import info.td.common.ui.UIElements.{IconAndTooltip, ImageIcon}

class ImageIconSwingBridge private(val component: JPanel) extends ImageIcon {
  def this() = this(new JPanel(new BorderLayout()))

  component.setMinimumSize(new Dimension(16, 16))

  override def setIcon(iconAndTooltip: IconAndTooltip) {
    component.removeAll()
    val labelWithImage = SwingHelpers.labelWithImage(iconAndTooltip.icon)
    component.add(labelWithImage, BorderLayout.CENTER)
    if (iconAndTooltip.tooltip.string.nonEmpty)
      labelWithImage.setToolTipText(iconAndTooltip.tooltip.string)
    component.revalidate()
    component.repaint()
  }
}
