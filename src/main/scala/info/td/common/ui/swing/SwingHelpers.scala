package info.td.common.ui.swing

import java.awt._
import java.awt.event._

import _root_.rx.lang.scala.{Observable, Subscription}
import com.typesafe.scalalogging.LazyLogging
import info.td.common._
import javax.swing._
import javax.swing.border.{CompoundBorder, EmptyBorder}
import javax.swing.plaf.FontUIResource
import net.miginfocom.swing.MigLayout

import scala.annotation.tailrec

object SwingHelpers extends LazyLogging {
  def labelWithImage(imageResourcePath: String): JLabel = {
    new JLabel("", imageIcon(imageResourcePath), 0)
  }

  @tailrec
  def findWindowOf(c: Component): Option[Window] = {
    if (c == null) {
      Some(JOptionPane.getRootFrame)
    } else c match {
      case w: Window => Some(w)
      case other => findWindowOf(other.getParent)
    }
  }

  def fitJTableRowHeightToFont(table: JTable) {
    invokeLater {
      UIManager.getDefaults.get("Table.font") match {
        case font: Font => table.setRowHeight((font.getSize * 1.5).toInt)
        case _ =>
      }
    }
  }

  @tailrec
  final def getScrollPane(component: Component): Option[JScrollPane] = {
    component match {
      case null => None
      case scrollPane: JScrollPane => Some(scrollPane)
      case other => getScrollPane(other.getParent)
    }
  }

  def scrollToRow(table: JTable, row: Int, column: Int) {
    getScrollPane(table) foreach { scrollPane =>
      val viewport = scrollPane.getViewport
      val r = table.getCellRect(row, column, true)
      val p = viewport.getViewPosition
      r.setLocation(r.x - p.x, r.y - p.y + table.getRowHeight)
      viewport.scrollRectToVisible(r)
    }
  }

  def imageIcon(imageResourcePath: String): ImageIcon = {
    val imageResource = getClass.getResource(imageResourcePath)
    if (imageResource == null) {
      logger.error(s"Could not find image resource at $imageResourcePath")
      new ImageIcon()
    } else {
      new ImageIcon(imageResource)
    }
  }

  def scaleFont(component: JComponent, scale: Double) {
    val oldFont = component.getFont
    val newFont = oldFont.deriveFont((oldFont.getSize * scale).toFloat)
    component.setFont(newFont)
  }

  def setMonospacedFont(component: JComponent): Unit = {
    component.setFont(
      new Font("Monospaced", Font.BOLD, component.getFont.getSize)
    )
  }

  def scaleFontSizes(multiplier: Float) {
    val defaults = UIManager.getDefaults
    import scala.jdk.CollectionConverters._
    for (key <- defaults.keys().asScala) {
      val value = defaults.get(key)
      value match {
        case font: Font => {
          val newSize = Math.round(font.getSize * multiplier)
          if (value.isInstanceOf[FontUIResource]) {
            defaults.put(key, new FontUIResource(font.getName, font.getStyle, newSize))
          } else {
            defaults.put(key, new Font(font.getName, font.getStyle, newSize))
          }
        }
        case _ =>
      }
    }
  }

  def swingAction(action: ActionEvent => Unit): AbstractAction = (e: ActionEvent) => {
    action(e)
  }

  def buttonClickAction(button: AbstractButton): AbstractAction = swingAction(_ =>
    if (button.isEnabled)
      button.doClick()
  )

  def bindKeyAction(component: JComponent, stroke: KeyStroke, action: AbstractAction) {
    component.getActionMap.put(action, action)
    component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(stroke, action)
  }

  def unbindKeyAction(component: JComponent, stroke: KeyStroke, action: AbstractAction): Unit = {
    component.getActionMap.remove(action)
    component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).remove(stroke)
  }

  def addPopupListener(component: JComponent, jPopupMenu: JPopupMenu) {
    component.addMouseListener(new MouseListener {
      private[this] def maybeShowPopup(e: MouseEvent) {
        if (e.isPopupTrigger) {
          jPopupMenu.show(e.getComponent, e.getX, e.getY)
        }
      }

      override def mouseClicked(e: MouseEvent): Unit = {}

      override def mouseExited(e: MouseEvent): Unit = {}

      override def mouseEntered(e: MouseEvent): Unit = {}

      override def mousePressed(e: MouseEvent): Unit = maybeShowPopup(e)

      override def mouseReleased(e: MouseEvent): Unit = maybeShowPopup(e)
    })
  }


  def installPanelWithPanel(hostContainer: Container, panelToEmbed: Component) {
    SwingHelpers.invokeOnEDT {
      hostContainer.removeAll()
      hostContainer.setLayout(new MigLayout("insets 0, fill", "[fill]", "[fill]"))
      hostContainer.add(panelToEmbed)
      hostContainer.revalidate()
      hostContainer.repaint()
    }
  }

  /**
   * Warning, this label don't have select/copy ability, doesn't wrap, it is just a plain JLabel!
   */
  def bigLabel(text: String, scale: Double = 2.0): JLabel = {
    val label = new JLabel(text)
    SwingHelpers.scaleFont(label, scale)
    label
  }

  def framedMonospacedLabel(fontSize: Int, initialString: String): UIMessageSwingBridge = {
    val font = new Font("Monospaced", Font.BOLD, fontSize)
    val label = UIMessageSwingBridge.textField
    label.textComponent.setFont(font)
    label.textComponent.setBorder(lineBorder())
    label
  }

  def lineBorder(color: Color = Color.DARK_GRAY) =
    new CompoundBorder(
      BorderFactory.createLineBorder(color),
      new EmptyBorder(0, 0, 0, 0)
    )

  /** Please note, that action will ALWAYS be invoked later, even in the case when we are already running on EDT.
   * If you just want to make sure your code will run on the EDT, just use invokeOnEDT() */
  def invokeLater(action: => Unit): Unit = {
    SwingUtilities.invokeLater(() => action)
  }

  def invokeOnEDT(action: => Unit): Unit = {
    if (SwingUtilities.isEventDispatchThread) {
      action
    } else {
      SwingUtilities.invokeLater(() => action)
    }
  }

  def setLocationTo(window: Window, position: Point): Unit = {
    val screenBounds = GraphicsEnvironment.getLocalGraphicsEnvironment.getMaximumWindowBounds
    val maxX = screenBounds.width - window.getWidth
    val maxY = screenBounds.height - window.getHeight
    window.setLocation(position.x min maxX, position.y min maxY)
  }

  def setLocationToCursor(window: Window): Unit = setLocationTo(window, MouseInfo.getPointerInfo.getLocation)
}

trait JFrameHelpers {
  window: Window =>

  def getRootPane: JRootPane // defined by both JFrame and JDialog

  /** {{{setIconFromResource("/icons/mmd-48.png")}}} */
  def setIconFromResource(resourcePath: String) {
    val image = SwingHelpers.imageIcon(resourcePath).getImage
    setIconImage(image)
  }

  lazy val buttonClose: JButton = {
    val button = new JButton(i18n.tc("button", "Close"))
    button.addActionListener((_: ActionEvent) => {
      close()
    })
    button
  }

  def closeOnEscape() {
    val escListener = new ActionListener() {
      override def actionPerformed(e: ActionEvent): Unit = {
        close()
      }
    }
    getRootPane.registerKeyboardAction(escListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW)
  }

  def closeOnLostFocus() {
    window.addWindowFocusListener(new WindowFocusListener {
      def windowLostFocus(windowEvent: WindowEvent) {
        close()
      }

      def windowGainedFocus(windowEvent: WindowEvent) {}
    })
  }

  def doOnWindowClosed(action: => Unit): Unit = {
    window.addWindowListener(new WindowListener {
      def windowOpened(windowEvent: WindowEvent): Unit = {}

      def windowDeiconified(windowEvent: WindowEvent): Unit = {}

      def windowClosing(windowEvent: WindowEvent): Unit = {}

      def windowClosed(windowEvent: WindowEvent) {
        action
      }

      def windowActivated(windowEvent: WindowEvent): Unit = {}

      def windowDeactivated(windowEvent: WindowEvent): Unit = {}

      def windowIconified(windowEvent: WindowEvent): Unit = {}
    })
  }

  def close() {
    dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING))
  }

  def closeWindowRequestSource: Observable[Unit] = Observable[Unit](observer => {
    val windowListener = new WindowAdapter() {
      override def windowClosing(e: WindowEvent) {
        observer.onNext(())
      }
    }
    addWindowListener(windowListener)
    Subscription {
      removeWindowListener(windowListener)
    }
  })

  def windowClosedSource: Observable[Unit] = {
    Observable(subscriber => doOnWindowClosed {
      subscriber.onNext(())
      subscriber.onCompleted()
    })
  }

  protected def bindKeyAction(stroke: KeyStroke, action: AbstractAction): Unit = {
    SwingHelpers.bindKeyAction(getRootPane, stroke, action)
  }

  protected def unbindKeyAction(stroke: KeyStroke, action: AbstractAction): Unit = {
    SwingHelpers.unbindKeyAction(getRootPane, stroke, action)
  }
}