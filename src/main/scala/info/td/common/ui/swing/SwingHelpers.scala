package info.td.common.ui.swing

import com.typesafe.scalalogging.LazyLogging
import net.miginfocom.swing.MigLayout

import java.awt._
import java.awt.event._
import javax.swing._
import scala.annotation.tailrec

object SwingHelpers extends LazyLogging {
  @tailrec
  def findWindowOf(c: Component): Option[Window] = {
    if (c == null) {
      Some(JOptionPane.getRootFrame)
    } else c match {
      case w: Window => Some(w)
      case other => findWindowOf(other.getParent)
    }
  }

  def fitJTableRowHeightToFont(table: JTable): Unit = {
    invokeLater {
      UIManager.getDefaults.get("Table.font") match {
        case font: Font => table.setRowHeight((font.getSize * 1.5).toInt)
        case _ =>
      }
    }
  }

  def scrollToRow(table: JTable, row: Int, column: Int): Unit = {
    @tailrec
    def getScrollPane(component: Component): Option[JScrollPane] = {
      component match {
        case null => None
        case scrollPane: JScrollPane => Some(scrollPane)
        case other => getScrollPane(other.getParent)
      }
    }

    getScrollPane(table) foreach { scrollPane =>
      val viewport = scrollPane.getViewport
      val r = table.getCellRect(row, column, true)
      val p = viewport.getViewPosition
      r.setLocation(r.x - p.x, r.y - p.y + table.getRowHeight)
      viewport.scrollRectToVisible(r)
    }
  }

  def imageIcon(imageResourcePath: String): ImageIcon = {
    val imageResource = getClass.getResource(imageResourcePath)
    if (imageResource == null) {
      logger.error(s"Could not find image resource at $imageResourcePath")
      new ImageIcon()
    } else {
      new ImageIcon(imageResource)
    }
  }

  def scaleFont(component: JComponent, scale: Double): Unit = {
    val oldFont = component.getFont
    val newFont = oldFont.deriveFont((oldFont.getSize * scale).toFloat)
    component.setFont(newFont)
  }

  def swingAction(action: ActionEvent => Unit): AbstractAction = (e: ActionEvent) => {
    action(e)
  }

  def buttonClickAction(button: AbstractButton): AbstractAction = swingAction(_ =>
    if (button.isEnabled)
      button.doClick()
  )

  def bindKeyAction(component: JComponent, stroke: KeyStroke, action: AbstractAction): Unit = {
    component.getActionMap.put(action, action)
    component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(stroke, action)
  }

  def unbindKeyAction(component: JComponent, stroke: KeyStroke, action: AbstractAction): Unit = {
    component.getActionMap.remove(action)
    component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).remove(stroke)
  }

  def addPopupListener(component: JComponent, jPopupMenu: JPopupMenu): Unit = {
    component.addMouseListener(new MouseListener {
      private[this] def maybeShowPopup(e: MouseEvent): Unit = {
        if (e.isPopupTrigger) {
          jPopupMenu.show(e.getComponent, e.getX, e.getY)
        }
      }

      override def mouseClicked(e: MouseEvent): Unit = {}

      override def mouseExited(e: MouseEvent): Unit = {}

      override def mouseEntered(e: MouseEvent): Unit = {}

      override def mousePressed(e: MouseEvent): Unit = maybeShowPopup(e)

      override def mouseReleased(e: MouseEvent): Unit = maybeShowPopup(e)
    })
  }


  def installPanelWithPanel(hostContainer: Container, panelToEmbed: Component): Unit = {
    SwingHelpers.invokeOnEDT {
      hostContainer.removeAll()
      hostContainer.setLayout(new MigLayout("insets 0, fill", "[fill]", "[fill]"))
      hostContainer.add(panelToEmbed)
      // This is very important.
      // When we are changing a LaF while the panelToEmbed was hidden, it won't reflect the new LaF
      // Explicitly force it to be sure.
      SwingUtilities.updateComponentTreeUI(hostContainer)
    }
  }

  /**
   * Warning, this label don't have select/copy ability, doesn't wrap, it is just a plain JLabel!
   */
  def bigLabel(text: String, scale: Double = 2.0): JLabel = {
    val label = new JLabel(text)
    SwingHelpers.scaleFont(label, scale)
    label
  }

  /** Please note, that action will ALWAYS be invoked later, even in the case when we are already running on EDT.
   * If you just want to make sure your code will run on the EDT, just use invokeOnEDT() */
  def invokeLater(action: => Unit): Unit = {
    SwingUtilities.invokeLater(() => action)
  }

  def invokeOnEDT(action: => Unit): Unit = {
    if (SwingUtilities.isEventDispatchThread) {
      action
    } else {
      SwingUtilities.invokeLater(() => action)
    }
  }

  def setLocationTo(window: Window, position: Point): Unit = {
    val screenBounds = GraphicsEnvironment.getLocalGraphicsEnvironment.getMaximumWindowBounds
    val maxX = screenBounds.width - window.getWidth
    val maxY = screenBounds.height - window.getHeight
    window.setLocation(position.x min maxX, position.y min maxY)
  }

  def setLocationToCursor(window: Window): Unit = setLocationTo(window, MouseInfo.getPointerInfo.getLocation)

  /** There are basically two approaches to negating the image (brightness) while preserving colors:
   *
   * 1. Negate the image totally (that is, invert the Red, Green, Blue components), then turn Hue 180°
   * 2. Invert the Value component of the HSV.
   *
   * Each comes with it's own problems:
   *
   * The problem with 1. is that the darker green colors (e. g. the rail in immediate-values*.png) turn immensely bright
   * The problem with 2. is that the light colors (e. g. red in immediate-values*.png that has lightness 1.0) disappear completely
   *
   * So we use both and combine the results. We prefer 2. but in places where the color has disappeared, we replace it by value obtained by 1.
   */
  def colorAdaptedToDarkLaf(argb: Int): Int = {
    val hsvTmp: Array[Float] = new Array(3)

    def negateAndFixHue(rgb: Integer): Integer = {
      val r = rgb >> 16 & 255
      val g = rgb >> 8 & 255
      val b = rgb >> 0 & 255

      // Negate RGB and convert it to HSV
      Color.RGBtoHSB(255 - r, 255 - g, 255 - b, hsvTmp)

      // Fix negated Hue by turning it 180°
      val hue = {
        val hue = hsvTmp(0) + 0.5f
        if (hue > 1.0f) hue - 1.0f else hue
      }

      // Go back to RGB
      Color.HSBtoRGB(
        hue,
        hsvTmp(1),
        hsvTmp(2),
      )
    }

    def invertLightness(rgb: Integer): Integer = {
      Color.RGBtoHSB(rgb >> 16 & 255, rgb >> 8 & 255, rgb >> 0 & 255, hsvTmp)

      val invertedLightness = 1.0f - hsvTmp(2)

      // Apply some gamma to brighten it up a bit. The `if` is just an optimization
      val lightnessWithGamma = if (invertedLightness == 0) 0 else Math.pow(invertedLightness.toDouble, 0.6).toFloat

      Color.HSBtoRGB(hsvTmp(0), hsvTmp(1), lightnessWithGamma)
    }

    /* Averaging doesn't look that good
    @inline
    def average(rgb1: Integer, c1: Double, rgb2: Integer, c2: Double) = {
      ((((rgb1 >> 16) & 0xFF).toDouble * c1 + ((rgb2 >> 16) & 0xFF).toDouble * c2).toInt.min(0xFF) << 16) |
        ((((rgb1 >> 8) & 0xFF).toDouble * c1 + ((rgb2 >> 8) & 0xFF).toDouble * c2).toInt.min(0xFF) << 8) |
        ((((rgb1 >> 0) & 0xFF).toDouble * c1 + ((rgb2 >> 0) & 0xFF).toDouble * c2).toInt.min(0xFF) << 0)
    }
    val result = average(invertLightness(argb), 0.5, negateAndFixHue(argb), 0.5)
    */

    // Prefer invertLightness, but use negateAndFixHue where invertLightness leads to disappeared pixels.
    val invertedRgb = invertLightness(argb)
    val correctedInverted = if ((invertedRgb & 0xFFFFFF) == 0) {
      negateAndFixHue(argb)
    } else {
      invertedRgb
    }

    val withFixedAlpha = correctedInverted & 0x00FFFFFF | (argb & 0xFF000000)

    withFixedAlpha
  }

  def isDarkLaf: Boolean = {
    @inline
    def pseudoBrightness(color: Color): Int = {
      color.getRed + color.getGreen + color.getBlue
    }

    val textField = new JTextField()
    pseudoBrightness(textField.getBackground) < pseudoBrightness(textField.getForeground)
  }

  def colorAdaptedToDarkLightLaf(color: Color): Color = {
    if (isDarkLaf) {
      val argb = color.getAlpha << 24 | color.getRed << 16 | color.getGreen << 8 | color.getBlue
      val inverted = colorAdaptedToDarkLaf(argb)
      new Color(
        inverted >> 16 & 255,
        inverted >> 8 & 255,
        inverted >> 0 & 255,
        inverted >> 24 & 255,
      )
    } else {
      color
    }
  }

}
