package info.td.common.ui.swing

import java.awt.{Component, Font}
import java.text.MessageFormat
import java.util.Locale

import _root_.rx.lang.scala.{Observable, Subject}
import com.typesafe.scalalogging.LazyLogging
import info.td.common._
import info.td.common.exceptions.UserError
import info.td.common.rx.NonFailingSubscribeErrorHandler
import info.td.common.rx.RxHelpers._
import info.td.common.ui.UIMessage
import info.td.mv.{Model, View, ViewSources}
import javax.swing._
import javax.swing.border.EtchedBorder
import javax.swing.event.ListSelectionEvent
import javax.swing.table.{AbstractTableModel, DefaultTableCellRenderer}
import net.miginfocom.swing.MigLayout

class SwingUIMessageListFrame(protected val titleString: String) extends JFrame with JFrameHelpers {
  val messageListPanel = new SwingUIMessageListPanel

  def selectMessage(message: UIMessage): Unit = messageListPanel.selectMessage(message)

  def create() {
    setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE)
    setTitle(titleString)

    val mainPanel = getContentPane

    mainPanel.setLayout(new MigLayout("fill", "[fill]", "[grow,fill][]"))
    mainPanel.add(messageListPanel, "wrap")
    mainPanel.add(buttonClose, "tag close")
    pack()

    closeOnEscape()
  }

  create()
}

case class SwingUIMessageListPanelViewSources(
  selectedMessageSource: Observable[UIMessage],
  unreadMessagesSource: Observable[Set[UIMessage]]
) extends ViewSources

case class SwingUIMessageListPanelModel(uiMessagesSource: Observable[UIMessage]) extends Model

class SwingUIMessageListPanel
  extends JPanel
    with View[SwingUIMessageListPanelModel, SwingUIMessageListPanelViewSources]
    with LazyLogging {
  private val selectedMessageSubject = Subject[UIMessage]()

  private val allSelectedMessagesSource: Observable[Set[UIMessage]] =
    selectedMessageSubject
      .distinctUntilChanged
      .scan(Set.empty[UIMessage])(_ + _)

  private var unreadMessages: Set[UIMessage] = Set.empty

  private[this] val uiMessagesSubject = Subject[UIMessage]()

  val viewSources: SwingUIMessageListPanelViewSources = SwingUIMessageListPanelViewSources(
    selectedMessageSubject.distinctUntilChanged,
    uiMessagesSubject
      .scan(Set.empty[UIMessage])(_ + _)
      .combineLatestWith(Observable.just(Set.empty) ++ allSelectedMessagesSource)((allMessages, selectedMessages) => allMessages -- selectedMessages)
  )

  private val thisLocale = Locale.getDefault

  private sealed abstract class ColumnDefinition {
    def name: String

    def cellString(uiMessage: UIMessage): String

    def preferredWidth: Option[Int] = None
  }

  private object ColumnDefinitionTimestamp extends ColumnDefinition {
    def name: String = i18n.t("Time")

    def cellString(uiMessage: UIMessage): String =
      new MessageFormat("{0,date,short} {0,time}", thisLocale).format(Array(uiMessage.timestamp))

    override def preferredWidth: Option[Int] = Some(15)
  }

  private object ColumnDefinitionType extends ColumnDefinition {
    def name: String = i18n.t("Type")

    def cellString(uiMessage: UIMessage): String = if (uiMessage.isInfo) {
      i18n.t("info")
    } else if (uiMessage.isWarning) {
      i18n.t("warning")
    } else if (uiMessage.isError) {
      i18n.t("error")
    } else {
      logger.error(s"cellString: Unknown message type in $uiMessage")
      ""
    }

    override def preferredWidth: Option[Int] = Some(80)
  }

  private object ColumnDefinitionMessage extends ColumnDefinition {
    def name: String = i18n.t("Message")

    def cellString(uiMessage: UIMessage): String = uiMessage.string.takeWhile(_ != '\n')
  }

  private val columnDefinitions = Array(ColumnDefinitionTimestamp, ColumnDefinitionType, ColumnDefinitionMessage)

  private class CustomRenderer extends DefaultTableCellRenderer {
    override def getTableCellRendererComponent(
      table: JTable, value: Object,
      isSelected: Boolean, hasFocus: Boolean,
      row: Int, column: Int): Component = {
      val cellComponent = super.getTableCellRendererComponent(table, value, isSelected, false, row, column)
      val message = messageAtRow(row)
      if (unreadMessages contains message) {
        cellComponent.setFont(cellComponent.getFont.deriveFont(Font.BOLD))
      }

      if (message.isInfo)
        cellComponent setBackground null
      if (message.isWarning)
        cellComponent setBackground UIMessageSwingBridge.warningColor
      else if (message.isError)
        cellComponent setBackground UIMessageSwingBridge.errorColor

      if (isSelected) {
        val panel = new JPanel(new MigLayout("insets 0, fill", "[fill]", "[fill]"))
        panel.add(cellComponent)
        if (isSelected)
          panel.setBorder(new EtchedBorder(0))
        panel
      } else {
        cellComponent
      }
    }
  }

  private class MessagesTableModel extends AbstractTableModel {
    private[this] var _messages: Vector[UIMessage] = Vector.empty

    def messages: Seq[UIMessage] = _messages

    def addRow(item: UIMessage): Unit = {
      val row = messages.size
      _messages +:= item
      fireTableRowsInserted(row, row)
    }

    def getValueAt(rowIndex: Int, columnIndex: Int): String = {
      val uiMessage = try {
        messages(rowIndex)
      } catch {
        case _: Throwable => {
          logger.warn(s"getValueAt: Unreachable message at index $rowIndex")
          UIMessage.info("")
        }
      }
      columnDefinitions(columnIndex).cellString(uiMessage)
    }

    def getColumnCount: Int = columnDefinitions.length

    override def isCellEditable(rowIndex: Int, columnIndex: Int): Boolean = false

    def getRowCount: Int = messages.size

    override def getColumnName(column: Int): String = columnDefinitions(column).name
  }

  private[this] val tableModel = new MessagesTableModel

  def messages: Seq[UIMessage] = tableModel.messages

  private[this] val messagesTable = {
    val table = new JTable(tableModel)
    SwingHelpers.fitJTableRowHeightToFont(table)
    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION)
    table
  }

  //  new TableColumnAdjuster(messagesTable, true, false, true, true)

  private[this] def messageAtRow(row: Int): UIMessage = messages.lift(row).getOrElse(UIMessage.info(""))

  private[this] val messagesScrollPane = {
    val scrollPane = new JScrollPane(messagesTable)
    scrollPane setBorder null
    scrollPane
  }

  private[this] val messagePanel = UIMessagePanelSwingBridge.messagePanel

  messagesTable.setFillsViewportHeight(true)
  columnDefinitions.indices
    .map(index => messagesTable.getColumnModel.getColumn(index))
    .foreach(column => column.setCellRenderer(new CustomRenderer))

  columnDefinitions.zipWithIndex.foreach {
    case (columnDefinition, index) =>
      columnDefinition.preferredWidth.foreach(width => {
        messagesTable.getColumnModel.getColumn(index).setMinWidth(width)
        messagesTable.getColumnModel.getColumn(index).setPreferredWidth(width)
      }
      )
  }

  val splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, false, messagesScrollPane, messagePanel.component)
  splitPane.setOneTouchExpandable(true)
  splitPane.setDividerLocation(200)

  messagesTable.getSelectionModel.addListSelectionListener((_: ListSelectionEvent) => {
    selectedMessageSubject.onNext(messageAtRow(messagesTable.getSelectedRow))
  })

  setLayout(new MigLayout("insets 0, noVisualPadding, fill", "[fill]", "[grow,fill][]"))
  add(splitPane, "wrap")

  def subscribeToModel(model: SwingUIMessageListPanelModel)(implicit schedulerProvider: SchedulerProvider): Unit = {
    viewSources.unreadMessagesSource.subscribe(unreadMessages = _)
    model.uiMessagesSource subscribeToSubject uiMessagesSubject
    messagePanel.subscribeToMessages(viewSources.selectedMessageSource)

    model.uiMessagesSource
      .observeOn(schedulerProvider.swing)
      .subscribe(next => {
        tableModel.addRow(next)
        if (messagesTable.getSelectedRow != -1) {
          selectRow(messagesTable.getSelectedRow + 1)
        }
        messagesTable.revalidate()
        messagesTable.repaint()
      }, NonFailingSubscribeErrorHandler)
  }

  private[this] def selectRow(index: Int) {
    try {
      messagesTable.setRowSelectionInterval(index, index)
    } catch {
      case _: IllegalArgumentException => logger.error("selectMessage")
    }
  }

  def selectMessage(message: UIMessage) {
    selectMessageThat(_ isProbablySameAs message)
  }

  def selectMessageThat(predicate: UIMessage => Boolean) {
    messages.indexWhere(predicate) match {
      case -1 =>
      case index => {
        selectRow(index)
        SwingHelpers.scrollToRow(messagesTable, index, 0)
      }
    }
  }
}

object SwingUIMessageListFrameTest {
  def main(args: Array[String]): Unit = {
    System.setProperty("awt.useSystemAAFontSettings", "on")
    System.setProperty("swing.aatext", "true")

    import scala.concurrent.duration._
    val frame = new SwingUIMessageListFrame("SwingUIMessageListFrameTest")
    frame.messageListPanel.subscribeToModel(SwingUIMessageListPanelModel(
      Observable.just(UIMessage.error("Error message", UserError("Details about the error", new RuntimeException("Bang!")))) ++
        Observable.from(Seq(
          UIMessage.info("Info message"),
          UIMessage.warning("Warning message"),
          UIMessage.error("Error message", UserError("Details about the error"))
        ))
          .repeat(5)
          .zip(Observable.interval(2.seconds))
          .map { case (message, index) => message.mapString(_ + " " + index.toString) }
    ))(productionSchedulerProvider)

    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
    frame.setVisible(true)
  }
}

