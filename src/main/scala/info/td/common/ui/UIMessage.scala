package info.td.common.ui

import java.util.Date

import com.typesafe.scalalogging.Logger
import info.td.common.rx.NonFailingSubscribeErrorHandler
import org.slf4j.Marker
import rx.lang.scala.Notification.{OnCompleted, OnError, OnNext}
import rx.lang.scala.Observable

case class UIMessage(string: String, messageType: UIMessage.MessageType = UIMessage.Info, timestamp: Date = new Date()) {

  import info.td.common.ui.UIMessage._

  def isInfo = messageType == Info

  def isWarning = messageType == Warning

  def isError = messageType.isInstanceOf[Error]

  def mapString(f: String => String) = UIMessage(f(string), messageType, timestamp)

  def withFirstLineOnly = mapString(_.takeWhile(_ != '\n'))

  def log(logger: Logger) {
    messageType match {
      case UIMessage.Info => logger.info(string)
      case UIMessage.Warning => logger.warn(string)
      case UIMessage.Error(maybeError) => maybeError match {
        case Some(error) => logger.error(string, error)
        case None => logger.error(string)
      }
    }
  }

  def log(logger: Logger, marker: Marker) {
    messageType match {
      case UIMessage.Info => logger.info(marker, string)
      case UIMessage.Warning => logger.warn(marker, string)
      case UIMessage.Error(maybeError) => maybeError match {
        case Some(error) => logger.error(marker, string, error)
        case None => logger.error(marker, string)
      }
    }
  }

  /* It can happen that a message in an e. g. status bar has been artificially cut in order to look nicer. But we still treat it as the same message.
  This is not completely clean, it doesn't e.g. cope with somebody using etcTo to shorten the message :( */
  def isProbablySameAs(that: UIMessage) = {
    this.timestamp == that.timestamp && this.messageType == that.messageType && (
      (this.string startsWith that.string) || (that.string startsWith this.string)
      )
  }
}

object UIMessage {

  abstract sealed class MessageType

  case object Info extends MessageType

  case object Warning extends MessageType

  case class Error(error: Option[Throwable]) extends MessageType

  def info(string: String) = UIMessage(string, Info)

  def warning(string: String) = UIMessage(string, Warning)

  def error(string: String, error: Throwable) = UIMessage(string, Error(Some(error)))

  def error(string: String) = UIMessage(string, Error(None))

  def sourceFrom[T](someSource: Observable[T], mapFunction: T => String, errorFunction: Throwable => String): Observable[UIMessage] = {
    Observable(observer => someSource.materialize.subscribe(
    {
      case OnNext(next) => observer.onNext(UIMessage.info(mapFunction(next)))
      case OnError(error) => {
        observer.onNext(UIMessage.error(errorFunction(error), error))
        observer.onCompleted()
      }
      case OnCompleted => {
        observer.onCompleted()
      }
    },
    NonFailingSubscribeErrorHandler)
    )
  }
}
