package info.td.common.ui

import java.io.File

import info.td.common.ui.UIElements.IconAndTooltip
import rx.lang.scala.{Observable, Subject}

object TestingUIElements {

  class Label(var text: String) {
    def setText(newText: String) {
      text = newText
    }
  }

  class Button() extends UIElements.Button {
    var enabled = true

    def setEnabled(newEnabled: Boolean) {
      enabled = newEnabled
    }

    def click(): Unit = {
      clicksSubject.onNext(())
    }

    private val clicksSubject = Subject[Unit]()

    def clicks: Observable[Unit] = clicksSubject
  }

  class FileChooser () extends UIElements.FileChooser {
    var enabled = true

    def setEnabled(newEnabled: Boolean) {
      enabled = newEnabled
    }

    override val fileSource = Subject[Seq[File]]()
  }

  class UIMessageLabel(var currentMessage: UIMessage = UIMessage("")) extends UIElements.UIMessageLabel {
    def setMessage(newMessage: UIMessage) {
      currentMessage = newMessage
    }

    override def setString(string: String): Unit = setMessage(UIMessage.info(string))
  }

  class ImageIcon(var iconAndTooltip: IconAndTooltip = IconAndTooltip("", UIMessage.info(""))) extends UIElements.ImageIcon {
    override def setIcon(newIconAndTooltip: IconAndTooltip): Unit = {
      iconAndTooltip = newIconAndTooltip
    }
  }
}
