package info.td.common.ui

import com.typesafe.scalalogging.LazyLogging
import rx.lang.scala.Observable
import info.td.common.rx.RxHelpers._
import java.io.File
import scala.concurrent.duration._

object UIElements extends LazyLogging {
  val subscribeThrottle = 200.milli

  trait ActiveElement {
    def setEnabled(enabled: Boolean): Unit

    def subscribeEnabledTo(enabledSource: Observable[Boolean])(implicit schedulerProvider: SchedulerProvider) {
      enabledSource
        .distinctUntilChanged
        .observeOn(schedulerProvider.swing)
        .subscribe(
          onNext => setEnabled(onNext),
          error => {
            logger.error(s"subscribeEnabledTo in ${this}: Unhandled error", error)
          })
    }
  }

  trait Button extends ActiveElement {
    def clicks: Observable[Unit]
  }

  trait FileChooser extends ActiveElement {
    def fileSource: Observable[Seq[File]]
  }

  trait StringLabel {
    protected[this] def setString(string: String)

    def subscribeToStrings(stringSource: Observable[String])(implicit schedulerProvider: SchedulerProvider) =
    // TODO About dialog in krtek doesn't work with that, investigate..
      stringSource. /*throttleLast(subscribeThrottle).*/ observeOn(schedulerProvider.swing).subscribe(
        string => setString(string),
        error => logger.error(s"subscribeToStrings in ${this} (StringLabel)", error)
      )
  }

  trait UIMessageLabel extends StringLabel {
    protected[this] def setMessage(message: UIMessage)

    def subscribeToMessages(messageSource: Observable[UIMessage])(implicit schedulerProvider: SchedulerProvider) = {
      messageSource. /*throttleLast(subscribeThrottle).*/ observeOn(schedulerProvider.swing).subscribe(
        message => setMessage(message),
        error => logger.error(s"subscribeToMessages in ${this} (UIMessageLabel)", error)
      )
    }
  }

  final case class IconAndTooltip(icon: String, tooltip: UIMessage)

  trait ImageIcon {
    def setIcon(iconAndTooltip: IconAndTooltip)

    def subscribeToIcons(iconSource: Observable[IconAndTooltip])(implicit schedulerProvider: SchedulerProvider) = {
      iconSource. /*throttleLast(subscribeThrottle).*/ observeOn(schedulerProvider.swing).subscribe(
        iconAndTooltip => setIcon(iconAndTooltip),
        error => logger.error(s"subscribeToIcons", error)
      )
    }
  }

}
