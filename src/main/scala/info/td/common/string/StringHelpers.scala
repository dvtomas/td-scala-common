package info.td.common.string

object StringHelpers {
  def etcTo(string: String, maxLength: Int): String = {
    val length = maxLength max 4
    if (string.length <= length)
      string
    else
      string.take(length - 3) + "..."
  }
}