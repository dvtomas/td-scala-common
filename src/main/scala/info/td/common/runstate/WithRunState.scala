package info.td.common.runstate

import java.util.concurrent.atomic.AtomicReference
import java.util.concurrent.locks.{Condition, ReentrantLock}

trait WithRunState {

  case class RunStateError(message: String = "", cause: Throwable = null) extends Exception(message, cause)

  abstract sealed class RunState

  case object Stopped extends RunState

  case object Running extends RunState

  case object Finished extends RunState

  case class Failed(error: Throwable) extends RunState

  private val _runState = new AtomicReference[RunState](Stopped)
  val runStateLock = new ReentrantLock()
  val runStateCondition: Condition = runStateLock.newCondition()

  def setRunState(newState: RunState) {
    runStateLock.lock()
    try {
      _runState.set(newState)
      runStateCondition.signalAll()
    } finally {
      runStateLock.unlock()
    }
  }

  def waitUntilRunning() {
    runStateLock.lock()
    try {
      while (_runState.get != Running) {
        _runState.get match {
          case Finished => throw new RunStateError("waitUntilRunning: Finished")
          case Failed(error) => throw new RunStateError("waitUntilRunning: Failed", error)
          case _ =>
        }
        runStateCondition.await()
      }
    } finally {
      runStateLock.unlock()
    }
  }

  def runState = runStateLock.synchronized(_runState.get)
}