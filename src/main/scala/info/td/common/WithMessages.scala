package info.td.common

import info.td.common.ui.UIMessage

import scala.collection.mutable.ArrayBuffer

case class WithMessages[+A](value: A, messages: List[UIMessage]) {
  def map[B](f: A => B): WithMessages[B] = WithMessages(f(value), messages)

  def flatMap[B](f: A => WithMessages[B]): WithMessages[B] = {
    val result = f(value)
    WithMessages(result.value, messages ++ result.messages)
  }

  def mapOption[Value, B](f: Value => B)(implicit ev: A <:< Option[Value]): WithMessages[Option[B]] = {
    WithMessages(value map f, messages)
  }

  def flatMapOption[Value, B](f: Value => WithMessages[Option[B]])(implicit ev: A <:< Option[Value]): WithMessages[Option[B]] = {
    ev(value) match {
      case None => WithMessages(None, messages)
      case Some(a) => {
        val result = f(a)
        WithMessages(result.value, messages ++ result.messages)
      }
    }
  }

  def flatMapSeq[Elem, B](f: Elem => WithMessages[Seq[B]])(implicit ev: A <:< Seq[Elem]): WithMessages[Seq[B]] = {
    var resultMessages = messages
    val resultList =
      ev(value).toVector /* We need to convert the source sequence to a strict one, since we need to evaluate all elements during the run of this method, to obtain all result messages. Try commenting out the .toVector and re-run the tests. */
        .flatMap { elem =>
          val result = f(elem)
          resultMessages = resultMessages ++ result.messages
          result.value
        }
    WithMessages(resultList, resultMessages)
  }

  def withAnotherMessage(message: UIMessage): WithMessages[A] = WithMessages(value, messages ++ Seq(message))

  def withClearedMessages: WithMessages[A] = WithMessages(value, Nil)
}

object WithMessages {
  def apply[A](value: A): WithMessages[A] = WithMessages(value, Nil)

  def apply[A](value: A, message: UIMessage): WithMessages[A] = WithMessages(value, message :: Nil)

  def traverse[A, B](values: Seq[A])(f: A ⇒ WithMessages[B]): WithMessages[Vector[B]] = {
    val resultMessages: ArrayBuffer[UIMessage] = new ArrayBuffer[UIMessage]
    val resultList: ArrayBuffer[B] = new ArrayBuffer[B]
    values foreach { value =>
      val resultWithMessages = f(value)
      resultList.+=(resultWithMessages.value)
      resultMessages ++= resultWithMessages.messages
    }
    new WithMessages(resultList.toVector, resultMessages.toList)
  }
}