package info.td.common.rx

import rx.lang.scala._
import rx.lang.scala.schedulers._
import rx.schedulers.{SwingScheduler => JavaSwingScheduler}

import scala.concurrent.duration.Duration

object SwingScheduler {

  def apply(): SwingScheduler = {
    new SwingScheduler(JavaSwingScheduler.getInstance())
  }
}

class SwingScheduler(val asJavaScheduler: rx.Scheduler) extends Scheduler {}

trait RxSchedulerHelpers {

  abstract class SchedulerProvider {
    def advanceTimeBy(duration: Duration): Unit

    def now: Long

    def immediate: Scheduler

    def trampoline: Scheduler

    def newThread: Scheduler

    def computation: Scheduler

    def io: Scheduler

    def swing: Scheduler
  }

  object productionSchedulerProvider extends SchedulerProvider {

    def advanceTimeBy(duration: Duration): Unit = {
      Thread.sleep(duration.toMillis)
    }

    def now: Long = trampoline.now

    def immediate: Scheduler = ImmediateScheduler()

    def trampoline: Scheduler = TrampolineScheduler()

    def newThread: Scheduler = NewThreadScheduler()

    def computation: Scheduler = ComputationScheduler()

    def io: Scheduler = IOScheduler()

    def swing: Scheduler = SwingScheduler()
  }

  class TestingSchedulerProvider extends SchedulerProvider {
    private lazy val scheduler = TestScheduler()

    def advanceTimeBy(duration: Duration): Unit = {
      scheduler.advanceTimeBy(duration)
    }

    def now: Long = scheduler.now

    def immediate: Scheduler = scheduler

    def trampoline: Scheduler = scheduler

    def newThread: Scheduler = scheduler

    def computation: Scheduler = scheduler

    def io: Scheduler = scheduler

    def swing: Scheduler = scheduler
  }

}