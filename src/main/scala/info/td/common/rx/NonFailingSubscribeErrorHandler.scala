package info.td.common.rx

import com.typesafe.scalalogging.LazyLogging

object NonFailingSubscribeErrorHandler extends (Throwable => Unit) with LazyLogging {
  def apply(error: Throwable): Unit = {
    logger.error("OnError in non-failing subscription", error)
  }
}