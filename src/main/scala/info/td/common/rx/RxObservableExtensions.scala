package info.td.common.rx

import com.typesafe.scalalogging.LazyLogging
import info.td.common.logging.TraceHelpers.{DumpFunction, printlnDumpFunction}
import rx.exceptions.MissingBackpressureException
import rx.lang.scala.Notification.{OnCompleted, OnError, OnNext}
import rx.lang.scala._
import rx.lang.scala.subjects.ReplaySubject

import scala.concurrent.duration.{Duration, _}
import scala.util.{Failure, Success, Try}

trait RxObservableExtensions {

  implicit class NotificationWrapper[T](val notification: Notification[T]) extends AnyRef {
    def map[R](f: T => R): Notification[R] = notification match {
      case OnNext(a) => OnNext(f(a))
      case OnCompleted => OnCompleted
      case OnError(e) => OnError(e)
    }
  }

  implicit class ObservableWrapper[T](val observable: Observable[T]) extends AnyRef {
    def forwardToSubject[U >: T](subject: Subject[U]): Observable[T] = {
      observable.doOnEach(subject.onNext, subject.onError, subject.onCompleted)
    }

    def subscribeToSubject[U >: T](subject: Subject[U]): Unit = {
      observable.subscribe(subject.onNext, subject.onError, subject.onCompleted)
    }

    def toTryObservable: Observable[Try[T]] = observable.materialize flatMap {
      case OnNext(elem) => Observable.just(Success(elem))
      case OnError(error) => Observable.just(Failure(error))
      case OnCompleted => Observable.empty
    }

    def toReplaySubject: ReplaySubject[T] = {
      val subject = ReplaySubject[T]()
      observable.subscribe(subject.onNext, subject.onError, subject.onCompleted)
      subject
    }

    /* F**k you @headinthebox ( https://github.com/Netflix/RxJava/issues/912 ) */
    def sampleWithObservable[ST](sampler: Observable[ST]): Observable[T] = {
      Observable[T] { subscriber =>
        var maybeCurrentItem: Option[T] = None
        val oSubscription = observable.subscribe(item => maybeCurrentItem = Some(item))
        var samplerSubscription: Subscription = null
        samplerSubscription = sampler.subscribe { _ =>
          if (subscriber.isUnsubscribed) {
            oSubscription.unsubscribe()
            Option(samplerSubscription) foreach (_.unsubscribe())
          } else {
            maybeCurrentItem foreach subscriber.onNext
          }
        }
      }
    }

    // Similar to sampleWithObservable, but also emit the original observable in the resulting tuple. Also, the implementation is more clever since I found it on the internets! :)
    def combinedWithLatestFrom[S](sampler: Observable[S]): Observable[(T, S)] = {
      val hotSampler = sampler.share
      observable.map(o => hotSampler.map(sample => (o, sample))).switch
    }

    def dump(name: String)(implicit dumpFunction: DumpFunction = printlnDumpFunction): Subscription = {
      observable.subscribe(
        next => dumpFunction(s"$name --> $next"),
        ex => dumpFunction(s"$name failed: $ex"),
        () => dumpFunction(s"$name completed"))
    }

    def dumpOnEach(name: String)(implicit dumpFunction: DumpFunction = printlnDumpFunction): Observable[T] =
      dumpOnEachMap(name, identity)

    def dumpOnEachMap[R](name: String, mapFunction: T => R)(implicit dumpFunction: DumpFunction = printlnDumpFunction): Observable[T] = {
      observable.doOnEach(
        next => dumpFunction(s"$name --> ${mapFunction(next)}"),
        ex => dumpFunction(s"$name failed: $ex"),
        () => dumpFunction(s"$name completed"))
    }

    def spy(name: String)(implicit dumpFunction: DumpFunction = printlnDumpFunction): Observable[T] =
      spyMap(name, identity)

    def spyMap[R](name: String, mapFunction: T => R)(implicit dumpFunction: DumpFunction = printlnDumpFunction): Observable[T] = {
      val id = s"[${Thread.currentThread().getName}] $name"
      observable.doOnEach(
        next => dumpFunction(s"$id --> ${mapFunction(next)}"),
        ex => dumpFunction(s"$id failed: $ex"),
        () => dumpFunction(s"$id completed")
      )
        .doOnSubscribe(dumpFunction(s"$id subscribe"))
        .doOnUnsubscribe(dumpFunction(s"$id unsubscribe"))
    }

    def timestampedFromNow(implicit schedulerProvider: RxSchedulerHelpers#SchedulerProvider): Observable[(Long, T)] = {
      val scheduler = schedulerProvider.immediate
      val now = scheduler.now
      observable
        .timestamp(scheduler)
        .map(timestampAndElem => (timestampAndElem._1 - now, timestampAndElem._2))
    }

    /** sample elements from queue through turnstile, one at a time
      *
      * @param turnstile For every `true` in the turnstile pass one element through from the queue
      * @return the source of queue elements passing through the turnstile
      */
    def throughTurnstile(turnstile: Observable[Boolean]): Observable[T] = {
      import scala.collection.immutable.Queue

      case class State(isOpen: Boolean, elementsInQueue: Queue[T], maybeLastEmittedElement: Option[T])
      sealed abstract class Transition
      case object Lock extends Transition
      case object Unlock extends Transition
      case class Element(element: T) extends Transition

      val initialState = State(isOpen = false, Queue.empty, None)

      observable.map(element => Element(element))
        .merge(turnstile map (unlock => if (unlock) Unlock else Lock))
        .scan(initialState) { case (State(isOpen, elementsInQueue, _), transition) => transition match {
          case Lock => State(isOpen = false, elementsInQueue, None)
          case Unlock => {
            if (elementsInQueue.isEmpty)
              State(isOpen = true, elementsInQueue, None)
            else {
              val (firstElement, newQueue) = elementsInQueue.dequeue
              State(isOpen = false, newQueue, Some(firstElement))
            }
          }
          case Element(newElement) => {
            if (isOpen) {
              if (elementsInQueue.isEmpty)
                State(isOpen = false, Queue.empty, Some(newElement))
              else {
                val (firstElement, newQueue) = elementsInQueue.dequeue
                State(isOpen = false, newQueue enqueue newElement, Some(firstElement))
              }
            } else {
              State(isOpen = false, elementsInQueue enqueue newElement, None)
            }
          }
        }
        }.collect { case State(_, _, Some(lastEmittedElement)) => lastEmittedElement }
    }

    /** http://stackoverflow.com/questions/29967263/how-to-implement-observeLatestOn-in-rxjava-rxscala */
    def observeLatestOn(scheduler: Scheduler): Observable[T] = {
      observable.bufferIntrospective(sendFinalBufferedItemsBeforeCompletion = false, scheduler).map(_.last)
    }

    /** Buffers items that may have arrived during a long-running onNext.
      *
      * http://stackoverflow.com/questions/28880247/buffer-while-processing-items
      * Implementation inspired by http://stackoverflow.com/questions/29967263/how-to-implement-observeLatestOn-in-rxjava-rxscala 
      * */
    def bufferIntrospective(sendFinalBufferedItemsBeforeCompletion: Boolean, scheduler: Scheduler): Observable[Seq[T]] = {
      observable.lift { child: Subscriber[Seq[T]] =>
        val worker = scheduler.createWorker
        child.add(worker)

        val parent: Subscriber[T] = new Subscriber[T] {

          private val lock = new AnyRef

          // protected by "lock"
          private var latest = List.empty[Notification[T]]
          // protected by "lock"
          // Means no task runs in the worker
          private var idle = true

          private var done: Boolean = false

          override def onStart(): Unit = {
            request(Long.MaxValue)
          }

          override def onNext(v: T): Unit = {
            if (!done) {
              emit(OnNext(v))
            }
          }

          override def onCompleted(): Unit = {
            if (!done) {
              done = true
              emit(OnCompleted)
            }
          }

          override def onError(e: Throwable): Unit = {
            if (!done) {
              done = true
              emit(OnError(e))
            }
          }

          def emit(v: Notification[T]): Unit = {
            var shouldSchedule = false
            lock.synchronized {
              if (idle) {
                // worker is idle so we should schedule a task
                shouldSchedule = true
                // We will schedule a task, so the worker will be busy
                idle = false
              }
              latest ::= v
            }
            if (shouldSchedule) {
              worker.schedule {
                var n: List[Notification[T]] = List.empty
                var exit = false
                while (!exit) {
                  lock.synchronized {
                    if (latest.isEmpty) {
                      // No new item arrives and we are leaving the worker, so set "idle"
                      idle = true
                      exit = true
                    } else {
                      n = latest
                      latest = List.empty
                    }
                  }

                  if (!exit) {
                    def onNextReversed(notifications: List[Notification[T]]): Unit = {
                      child.onNext(notifications.reverse.collect { case OnNext(item) => item })
                    }
                    n match {
                      case OnCompleted :: Nil => child.onCompleted()
                      case OnCompleted :: tail => {
                        if (sendFinalBufferedItemsBeforeCompletion) {
                          onNextReversed(tail)
                        }
                        child.onCompleted()
                      }
                      case OnError(error) :: Nil => child.onError(error)
                      case OnError(error) :: tail => {
                        if (sendFinalBufferedItemsBeforeCompletion) {
                          onNextReversed(tail)
                        }
                        child.onError(error)
                      }
                      case other => onNextReversed(other)
                    }
                  }
                }
              }
            }
          }
        }

        child.add(parent)

        parent
      }
    }

    /** Probe executes it's onNext, onCompleted and onError from within an executor -
     * user supplied lambda, so it will appear in the call stack in case of an error.
     * This may be very helpful with debugging.
     *
     * It also logs MissingBackpressureException-s.
     *
     * See also ProbeTest.
     * */

    def probe(name: String, executeFunc: (Notification[T] => Unit, Notification[T]) => Unit): Observable[T] = {
      observable.lift(s => new Subscriber[T](s) {
        //        private val probe = new Probe(name, executor)

        override def onStart(): Unit = {
          request(Long.MaxValue)
        }

        override def onCompleted(): Unit = {
          if (!s.isUnsubscribed) {
            executeFunc.apply((notification: Notification[T]) => notification.accept(s), OnCompleted)
          }
        }

        override def onError(error: Throwable): Unit = {
          if (!s.isUnsubscribed) {
            error match {
              case mbe: MissingBackpressureException => Probe.logMissingBackpressureException(name, mbe)
              case _ =>
            }
            executeFunc.apply((notification: Notification[T]) => notification.accept(s), OnError(error))
          }
        }

        override def onNext(value: T): Unit = {
          if (!s.isUnsubscribed) {
            executeFunc.apply((notification: Notification[T]) => {
              try {
                notification.accept(s)
              } catch {
                case mbe: MissingBackpressureException => Probe.logMissingBackpressureException(name, mbe)
              }
            },
              OnNext(value)
            )
          }
        }
      })
    }
  }


  def intervals(intervals: Seq[Duration], scheduler: Scheduler): Observable[Long] = {
    val beginning = scheduler.now
    intervals.foldLeft[Observable[Long]](Observable.empty) { case (o, duration) =>
      o ++ Observable.timer(duration, scheduler).take(1).map(_ => scheduler.now - beginning)
    }
  }

  def intervalsMilliSeconds(intervals: Seq[Int], scheduler: Scheduler): Observable[Long] = {
    this.intervals(intervals.map(_.milli), scheduler)
  }

  def pointsInTime(pointsInTime: Seq[Duration], scheduler: Scheduler): Observable[Long] = {
    val intervals = (Seq(0.milli) ++ pointsInTime) zip pointsInTime collect {
      case (a, b) => b - a
    }
    this.intervals(intervals, scheduler)
  }

  def pointsInTimeMilliSeconds(pointsInTime: Seq[Int], scheduler: Scheduler): Observable[Long] = {
    this.pointsInTime(pointsInTime.map(_.milli), scheduler)
  }
}

object Probe extends LazyLogging {
  def logMissingBackpressureException(name: String, mbe: MissingBackpressureException): Unit = {
    logger.error(s"MissingBackpressureException intercepted from within a probe '$name'", mbe)
  }
}