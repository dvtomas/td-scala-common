package info.td.common.rx

import scala.concurrent.Future
import rx.lang.scala.Observable
import rx.lang.scala.subjects.AsyncSubject
import scala.util.{Failure, Success}

trait FutureExtensions {
  implicit class FutureToObservable[T](val future: Future[T]) extends AnyRef {
    def toObservable(implicit executor: scala.concurrent.ExecutionContext): Observable[T] = {
      val subject = AsyncSubject[T]()

      future.onComplete {
        case Success(result) => {
          subject.onNext(result)
          subject.onCompleted()
        }
        case Failure(error) => subject.onError(error)
      }
      subject
    }
  }
}