package info.td

import com.typesafe.scalalogging.LazyLogging
import info.td.common.gettext.PoLoader
import info.td.common.gettext.PoLoader.PoDomain

package object common extends LazyLogging {
  val i18n = {
    val poWithMessages = PoLoader.load(PoDomain("TdScalaCommon"), autoEscapeSingleQuotes = true)
    poWithMessages.messages foreach (_.log(logger))
    poWithMessages.value
  }
}