package info.td.common.debug

object DebugHelpers {

  implicit class RichObject[T](o: T) {
    private[this] def print(s: String): Unit = {
      println(s"DBG $s")
    }

    def debugPrint(format: T => String = _.toString): T = {
      print(format(o))
      o
    }

    def debugPrint(string: String): T = {
      print(s"$string $o")
      o
    }
  }

}