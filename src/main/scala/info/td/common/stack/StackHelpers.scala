package info.td.common.stack

import java.io.{PrintWriter, StringWriter}

object StackHelpers {
  def printStackTraceString(): Unit = {
    println(getStackTraceString)
  }

  def getStackTraceString: String = {
    try {
      throw new RuntimeException("Foo")
    } catch {
      case e: Throwable => getStackTraceString(e)
    }
  }

  def getStackTraceString(throwable: Throwable): String = {
    val sw = new StringWriter()
    val pw = new PrintWriter(sw)
    throwable.printStackTrace(pw)
    sw.toString
  }
}