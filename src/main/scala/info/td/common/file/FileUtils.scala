package info.td.common.file

import java.io._

import com.typesafe.scalalogging.LazyLogging
import info.td.common._
import info.td.common.exceptions.UserError

import scala.io.Source
import scala.util.{Failure, Success, Try}

object FileUtils extends LazyLogging {
  def writeToFile(file: File, bytes: Array[Byte]): Try[Unit] = {
    var stream: FileOutputStream = null
    try {
      stream = new FileOutputStream(file)
      stream.write(bytes)
      stream.close()
      Success(())
    } catch {
      case e: Throwable => {
        Option(stream) foreach (_.close())
        Failure(e)
      }
    }
  }

  private[this] val UTF8_BOM = "\uFEFF"

  def stringFromUTF8File(file: File): Try[String] = {
    var source: Source = null
    try {
      source = Source.fromFile(file, "utf-8")
      val contents = source.mkString

      val contentsWithoutBOM = if (contents.startsWith(UTF8_BOM))
        contents.substring(1)
      else
        contents

      Success(contentsWithoutBOM)
    } catch {
      case e: IOException => Failure(UserError(i18n.tf("Error reading contents of text file {0}", safeCanonicalPath(file)), e))
    } finally {
      Option(source) foreach (_.close())
    }
  }

  /** Tries to delete the file or directory file. Logs problems as warnings via logback.
    * Returns Success if the removal was successful, Failure otherwise. */
  def deleteRecursively(file: File): Try[Unit] = {
    Try(file.isDirectory) match {
      case Success(true) => {
        Try(Option(file.listFiles())) match {
          case Success(Some(files)) => {
            for (f <- files) {
              deleteRecursively(f) match {
                case Success(()) =>
                case f@Failure(_) => return f
              }
            }
          }
          case Success(None) => return Failure(new IllegalArgumentException(s"deleteRecursively: Bad path: $file"))
          case Failure(err) => return Failure(err)
        }
      }
      case Success(false) =>
      case Failure(err) => return Failure(err)
    }

    Try(file.delete())
      .flatMap {
      case true => Success(())
      case false => Failure(new IOException(s"Failed to delete the file ${safeCanonicalPath(file)}"))
    }
  }

  def listFiles(directory: File): Try[Seq[File]] = {
    Try(Option(directory.list())) match {
      case Success(Some(files)) => Success(files.map(fileName => new File(directory, fileName)))
      case Success(None) => Failure(new IllegalArgumentException(s"listFiles: Bad path: $directory"))
      case Failure(err) => Failure(err)
    }
  }

  def safeCanonicalPath(file: File): String = {
    try {
      file.getCanonicalPath
    } catch {
      case e: IOException => {
        logger.warn(s"getCanonicalPath($file) failed", e)
        file.toString
      }
    }
  }

  /** Just a copyLarge from org.apache.commons.io.IOUtils */
  @throws(classOf[IOException])
  def copyLarge(input: InputStream, output: OutputStream): Long = {
    val bufferSize = 4096
    val buffer = new Array[Byte](bufferSize)
    var count = 0L

    var n1 = input.read(buffer, 0, bufferSize)
    while (n1 != -1) {
      count += n1
      output.write(buffer, 0, n1)
      output.flush()
      n1 = input.read(buffer, 0, bufferSize)
    }
    count
  }
}
