/**
 * Copyright 2014 Netflix, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rx.schedulers

import java.awt.EventQueue
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.util.concurrent.TimeUnit
import javax.swing.Timer
import rx.Scheduler
import rx.Scheduler.Worker
import rx.Subscription
import rx.functions.Action0
import rx.subscriptions.BooleanSubscription
import rx.subscriptions.CompositeSubscription
import rx.subscriptions.Subscriptions


final class SwingScheduler extends Scheduler {

  import SwingScheduler.InnerSwingScheduler

  def createWorker: Scheduler.Worker = {
    new InnerSwingScheduler
  }
}

/**
 * Executes work on the Swing UI thread.
 * This scheduler should only be used with actions that execute quickly.
 */
object SwingScheduler {
  private val INSTANCE: SwingScheduler = new SwingScheduler()

  def getInstance(): SwingScheduler = {
    INSTANCE
  }

  private class InnerSwingScheduler extends Worker {
    private final val innerSubscription: CompositeSubscription = new CompositeSubscription

    def unsubscribe() {
      innerSubscription.unsubscribe()
    }

    def isUnsubscribed: Boolean = {
      innerSubscription.isUnsubscribed
    }

    def schedule(action: Action0, delayTime: Long, unit: TimeUnit): Subscription = {
      val delay: Long = Math.max(0, unit.toMillis(delayTime))
      assertThatTheDelayIsValidForTheSwingTimer(delay)
      val s: BooleanSubscription = BooleanSubscription.create

      class ExecuteOnceAction extends ActionListener {
        private var timer: Timer = null

        private[InnerSwingScheduler] def setTimer(timer: Timer) {
          this.timer = timer
        }

        def actionPerformed(e: ActionEvent) {
          timer.stop()
          if (innerSubscription.isUnsubscribed || s.isUnsubscribed) {
            return
          }
          action.call()
          innerSubscription.remove(s)
        }
      }
      val executeOnce: ExecuteOnceAction = new ExecuteOnceAction
      val timer: Timer = new Timer(delay.toInt, executeOnce)
      executeOnce.setTimer(timer)
      timer.start()
      innerSubscription.add(s)
      Subscriptions.create(new Action0 {
        def call() {
          timer.stop()
          s.unsubscribe()
          innerSubscription.remove(s)
        }
      })
    }

    def schedule(action: Action0): Subscription = {
      val s: BooleanSubscription = BooleanSubscription.create
      EventQueue.invokeLater(new Runnable {
        def run() {
          if (innerSubscription.isUnsubscribed || s.isUnsubscribed) {
            return
          }
          action.call()
          innerSubscription.remove(s)
        }
      })
      innerSubscription.add(s)
      Subscriptions.create(new Action0 {
        def call() {
          s.unsubscribe()
          innerSubscription.remove(s)
        }
      })
    }
  }

  private def assertThatTheDelayIsValidForTheSwingTimer(delay: Long) {
    if (delay < 0 || delay > Integer.MAX_VALUE) {
      throw new IllegalArgumentException(s"The swing timer only accepts non-negative delays up to ${Integer.MAX_VALUE} milliseconds.")
    }
  }
}

