import sbt.Keys._
import sbt._
import sbtrelease.ReleasePlugin._
import sbtrelease.ReleaseStateTransformations

object BuildSettings {
  val buildSettings = Seq(
    organization := "info.td",
    scalaVersion := "2.13.4"
  )
}

object Resolvers {
}

object Dependencies {
  val dependencies = Seq(
    // Unit testing
    "com.lihaoyi" %% "utest" % "0.7.4" % "test",
    // Logging
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    // UI
    "com.miglayout" % "miglayout-swing" % "5.2",
    // Rx
    "io.reactivex" %% "rxscala" % "0.27.0",
    // parser combinators for PoLoader
    "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"
  )
}

object build extends Build {

  import BuildSettings._
  import Dependencies._

  //  val mainClassName = "com.measprog.mmd.gui.swing.Main"

  lazy val tdScalaCommon = Project(
    "td-scala-common",
    file("."),
    settings =
      buildSettings ++
        releaseSettings ++
        Seq(ReleaseKeys.releaseProcess := Seq[sbtrelease.ReleaseStep](
          ReleaseStateTransformations.checkSnapshotDependencies,
          ReleaseStateTransformations.inquireVersions,
          ReleaseStateTransformations.runTest,
          ReleaseStateTransformations.setReleaseVersion,
          ReleaseStateTransformations.commitReleaseVersion,
          ReleaseStateTransformations.tagRelease,
          // We do a publishLocal instead of publish to god knows where
          // ReleaseStateTransformations.publishArtifacts,
          sbtrelease.ReleaseStep(action = st => {
            val extracted = Project.extract(st)
            val ref = extracted.get(thisProjectRef)
            extracted.runAggregated(publishLocal in Global in ref, st)
          }),
          ReleaseStateTransformations.setNextVersion,
          ReleaseStateTransformations.commitNextVersion,
          ReleaseStateTransformations.pushChanges
        )) ++ Seq(
        scalacOptions ++= Seq("-feature", "-unchecked", "-deprecation"),
        libraryDependencies ++= dependencies,
        testFrameworks += new TestFramework("utest.runner.Framework")
      )
  )
}

