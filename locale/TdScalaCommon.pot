# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-21 11:26+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/main/scala/info/td/virtualkeyboard/SwingVirtualKeyboard.scala:405
msgid "Virtual keyboard"
msgstr ""

#: src/main/scala/info/td/common/ui/swing/StandardStreamsConsole.scala:137
msgid "Copy to clipboard"
msgstr ""

#: src/main/scala/info/td/common/ui/swing/SwingUIMessageListFrame.scala:83
msgid "Time"
msgstr ""

#: src/main/scala/info/td/common/ui/swing/SwingUIMessageListFrame.scala:92
msgid "Type"
msgstr ""

#: src/main/scala/info/td/common/ui/swing/SwingUIMessageListFrame.scala:95
msgid "info"
msgstr ""

#: src/main/scala/info/td/common/ui/swing/SwingUIMessageListFrame.scala:97
msgid "warning"
msgstr ""

#: src/main/scala/info/td/common/ui/swing/SwingUIMessageListFrame.scala:99
msgid "error"
msgstr ""

#: src/main/scala/info/td/common/ui/swing/SwingUIMessageListFrame.scala:109
msgid "Message"
msgstr ""

#: src/main/scala/info/td/common/ui/swing/SwingErrorDialog.scala:21
msgctxt "errorDialog"
msgid "An error has occurred"
msgstr ""

#: src/main/scala/info/td/common/ui/swing/SwingErrorDialog.scala:102
msgctxt "errorDialog"
msgid "No details"
msgstr ""

#: src/main/scala/info/td/common/ui/swing/SwingErrorDialog.scala:103
msgctxt "errorDialog"
msgid "Details"
msgstr ""

#: src/main/scala/info/td/common/ui/swing/SwingErrorDialog.scala:104
msgctxt "errorDialog"
msgid "All details"
msgstr ""

#: src/main/scala/info/td/common/ui/swing/SwingErrorDialog.scala:112
msgctxt "errorDialog"
msgid "Copy to clipboard"
msgstr ""

#: src/main/scala/info/td/common/ui/swing/SwingHelpers.scala:209
msgctxt "button"
msgid "Close"
msgstr ""

#: src/main/scala/info/td/common/file/FileUtils.scala:43
msgid "Error reading contents of text file {0}"
msgstr ""
